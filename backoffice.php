<?php
    session_start();
    include('class/autoload.php');
?>
<!DOCTYPE html>
<html lang="fr">
<?php
    require_once("imports/head-back-office.php");
?>
<body>
    <?php
        require_once("imports/menu-back-office.php");
        if (isset($_GET["page"]) && file_exists("imports/".$_GET["page"].".php")) {
            require_once("imports/".$_GET["page"].".php");
        } else {
            require_once("imports/utilisateurs-back-office.php");
        }

        require_once("imports/scripts-js.php");
    ?>
</body>
</html>