module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'), // sanilux
    watch: {
      sass: {
        files: 'assets/src/css/*.scss',
        tasks: ['sass']
      }
    },
    sass: {
      dist: {
        options: {
          style: 'compressed', // Can be nested, compact, compressed, expanded.
          compass: false
        },
        files: {
          'assets/css/styles.min.css': 'assets/src/css/styles.scss',
          'assets/css/commun.min.css': 'assets/src/css/commun.scss',
          'assets/css/header.min.css': 'assets/src/css/header.scss',
          'assets/css/footer.min.css': 'assets/src/css/footer.scss',
          'assets/css/scroll-top.min.css': 'assets/src/css/scroll-top.scss',
          'assets/css/accueil.min.css': 'assets/src/css/accueil.scss',
          'assets/css/user.min.css': 'assets/src/css/user.scss',
          'assets/css/login.min.css': 'assets/src/css/login.scss',
          'assets/css/collection.min.css': 'assets/src/css/collection.scss',
          'assets/css/produit.min.css': 'assets/src/css/produit.scss',
          'assets/css/user.min.css': 'assets/src/css/user.scss',
          'assets/css/panier.min.css': 'assets/src/css/panier.scss',
          'assets/css/back-office.min.css': 'assets/src/css/back-office.scss',
          'assets/css/faq.min.css': 'assets/src/css/faq.scss',
          'assets/css/modal.min.css': 'assets/src/css/modal.scss',
          'assets/css/badge.min.css': 'assets/src/css/badge.scss',
          'assets/css/form.min.css': 'assets/src/css/form.scss',
        }
      }
	  },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: ['assets/src/js/libs/*.js', 'assets/src/js/*.js'],
        dest: 'assets/js/script.min.js'
      }
    },
    imagemin: {
      dynamic: {
          files: [{
              expand: true,
              cwd: 'assets/src/img/',
              src: ['**/*.{png,jpg,gif}'],
              dest: 'assets/img/'
          }]
      }
    },
    clean: ['assets/js', 'assets/css']
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks("grunt-contrib-sass");
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-watch')

  // Default task(s).
  grunt.registerTask('default', ['clean', 'sass', 'uglify', 'imagemin']);

};
