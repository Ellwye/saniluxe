-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3309
-- Généré le :  ven. 07 fév. 2020 à 19:44
-- Version du serveur :  8.0.18
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `sanilux`
--

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'baignoires'),
(2, 'douches'),
(3, 'lavabos'),
(4, 'robinetterie'),
(5, 'toilettes');

-- --------------------------------------------------------

--
-- Structure de la table `panier`
--

DROP TABLE IF EXISTS `panier`;
CREATE TABLE IF NOT EXISTS `panier` (
  `id_user` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `panier`
--

INSERT INTO `panier` (`id_user`, `id_product`, `quantity`) VALUES
(5, 15, 1),
(6, 3, 2),
(6, 4, 2),
(6, 8, 1),
(8, 12, 1),
(8, 16, 1),
(8, 17, 1),
(7, 3, 1),
(7, 6, 1);

-- --------------------------------------------------------

--
-- Structure de la table `produits`
--

DROP TABLE IF EXISTS `produits`;
CREATE TABLE IF NOT EXISTS `produits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `prix` varchar(255) NOT NULL,
  `categorie` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `stock` int(11) NOT NULL,
  `visible` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `produits`
--

INSERT INTO `produits` (`id`, `titre`, `description`, `prix`, `categorie`, `image`, `stock`, `visible`) VALUES
(1, 'WC Bidet DEGRADE', 'Terri Pecora signe pour Simas une collection sanitaire aux formes élancées, sobres et qui permettent de gagner de la place. Simas propose pour cette collection des teintes sobres et \"extra mates\" offrant des WC et bidet disponible en noir.\r\nWC et Bidet à poser, en céramique, disponible en couleur blanc ou noir.', '1300', 'toilettes', 'assets/img/toilettes/degrade.jpg', 3, 1),
(2, 'WC Bidet EVO', 'WC et Bidet à poser, en céramique blanche', '1500', 'toilettes', 'assets/img/toilettes/evo.jpg', 10, 1),
(3, 'WC et Bidet, collection Link', 'WC Bidet suspendu, moderne, en céramique blanche.', '2500', 'toilettes', 'assets/img/toilettes/link.jpg', 12, 1),
(4, 'Baignoire Aegean', 'Baignoire en ilot, disponible en 2 dimensions:\r\n1800x825mm\r\n1700x825mm', '1500', 'baignoires', 'assets/img/baignoires/aegean.jpg', 40, 1),
(5, 'Baignoire en îlot REGAL', 'Devon & Devon propose la baignoire en îlot Regal 182 x 82 x 71cm, laissant un large espace pour un plus grand confort. Intérieur fonte émaillée, extérieur formé par une plaque d’aluminium astiquée. Baignoire disponible également avec feuille d’aluminium peinte en une des 213 couleurs de la Colors Collection de Devon&Devon.', '2500', 'baignoires', 'assets/img/baignoires/regal.jpg', 0, 1),
(6, 'Baignoire Kaos', 'Baignoire en méthacrylate free standing\r\n1850 x 1000 x h 510 mm', '2300', 'baignoires', 'assets/img/baignoires/kaos.jpg', 3, 1),
(7, 'Baignoire Eiffel rétro', 'La baignoire Eiffel est disponible avec un intérieur blanc en fonte émaillée et un extérieur couleur ou en cuivre. La version en cuivre au design rétro et élégant, ornée de rivets qui font référence aux ouvrages métalliques de Gustave Eiffel, apportera une touche raffinée à votre salle de bain. Avec sa capacité en eau de 215 litres et ses dimensions optimales, la baignoire Eiffel assure un grand confort et s’adapte facilement à tout type d’agencement. La fonte offre l’avantage de conserver l’eau du bain à bonne température.\r\n\r\n \r\n\r\nLes caractéristiques principales de la baignoire Recor Eiffel :\r\n\r\nLongueur : 1700 mm\r\nLargeur : 680 mm\r\nHauteur : 695 mm\r\nCapacité en eau : 215 litres\r\nPoids : 198 kg (vide)', '3260', 'baignoires', 'assets/img/baignoires/eiffel.jpg', 5, 1),
(8, 'Ciel de pluie avec éclairage Quadro Bossini', 'Ciel de pluie 300x300 mm autonettoyant, avec revêtement en plastique semi-transparent et éclairage à bas voltage. Installation au plafond', '4700', 'douches', 'assets/img/douches/quadro.jpg', 2, 1),
(9, 'Lavabo rétro en céramique Astoria', 'Imperial Bathroom propose dans un style rétro un lavabo design en céramique sur piètement en métal ou colonne céramique. La gamme Astoria s’inspire des années 1920 avec cette vasque de luxe aux formes géométriques. Disponible en 640 x 490 cm en blanc ou noir vernis, avec 1, 2 ou 3 trous percés, sur colonne ou sur pieds.', '580', 'lavabos', 'assets/img/lavabos/astoria.jpg', 4, 1),
(10, 'Lavabo retro La Chapelle Lefroy Brooks', 'Lavabo céramique sur colonne, style rétro pour une salle de bain de luxe et design.', '1200', 'lavabos', 'assets/img/lavabos/chapelle.jpg', 3, 1),
(11, 'Lavabo Metropole style Classic', 'La gamme Metropole s’inspire des vasques retro des années 1930, plus solide et avec le design angulaire qu’on lui connaît, pour une salle de bain de luxe retro. Lefroy Brooks propose une vasque design disponible en noir ou blanc, avec ou sans colonne ou sur pieds 613 x 496 mm.', '1300', 'lavabos', 'assets/img/lavabos/metropole.jpg', 5, 1),
(12, 'Vasque Ronde BetteCraft', 'Vasque faite intégralement comme le style d\'une baignoire, fabriquée en acier titane. \r\n\r\nDisponible en deux dimensions : \r\n\r\n350 x 350 x 120\r\n450 x 450 x 120', '2000', 'lavabos', 'assets/img/lavabos/bettecraft.jpg', 15, 1),
(13, 'Robinet Xenon - Samuel Heath', 'Robinet Samuel Heath de 3 trous sur gorge avec vidage et bec haut pivotant de 180 mm de hauteur et 100 mm de longueur. Fabriqué en Angleterre avec les matériaux les plus fins, le mélangeur 3 trous Samuel Heath, sur gorge, avec vidage s’inscrit dans un style plus contemporain. Disponible en chrome, noir mat et nickel satiné.\r\n\r\n', '200', 'robinetterie', 'assets/img/robinetterie/xenon.jpg', 5, 1),
(14, 'Robinet mélangeur Fairfield avec cristal Organique', 'Fabriqué en Angleterre avec les matériaux les plus fins, le mélangeur pour lavabo 3 trous Samuel Heath à manette, sur gorge, avec vidage à tirette s’inscrit dans un style classique pour une salle de bain d’un goût raffiné. Les manettes sont disponibles en métal, céramique, bois, cristal organique transparent ou noir, cristal géométrique transparent ou noir.', '300', 'robinetterie', 'assets/img/robinetterie/fairfield.jpg', 3, 1),
(15, 'Robinet inox, collection CIRCLE', 'Mélangeur mural avec douchette en caoutchouc de silicone.', '360', 'robinetterie', 'assets/img/robinetterie/circle.jpg', 12, 1),
(16, 'WC japonais, collection JAPONI', 'WC japonais suspendu en céramique blanche, moderne et plus hygiénique.\r\n\r\nLes avantages d\'un WC lavant sont nombreux :\r\n\r\nRinçage naturel et non agressif pour le corps puisqu\'il est fait uniquement avec de l\'eau.\r\nHygiène renforcée.\r\nSensation de propreté réelle, de fraîcheur et de bien-être tout au long de la journée.\r\nConfort d\'utilisation lié au chauffage de l\'eau et de l\'abattant.\r\n\r\nUn WC lavant peut aussi participer au maintien à domicile des personnes âgées, en augmentant leur autonomie et en réduisant les problèmes de mobilité et d\'accessibilité à un lavabo ou une douche après utilisation, du fait de la fonctionnalité rinçage intégrée au WC.', '5000', 'toilettes', 'assets/img/toilettes/japoni.jpg', 20, 1),
(17, 'Baignoire Scoop en Cristalplant', 'Falper a conçu la baignoire SCOOP en Cristalplant dans un style contemporain. Cette magnifique baignoire en Cristalplant ovale disponible en blanc ou bicolore est à positionner contre le mur. Son design original prévoit une plage pour accueillir une robinetterie sur gorge si voulue, ainsi que quelques accessoires, et offre un bon espace pour une réelle détente. Elle est disponible en blanc ou version bicolore avec intérieur blanc et une variation de 11 finitions partie extérieure.', '3500', 'baignoires', 'assets/img/baignoires/scoop.jpg', 15, 1),
(18, 'Ciel de pluie DREAM FLAT Bossini', 'Ciel de pluie avec lumières LED RGB, transformateur à bas voltage, clavier de control pour Cromothérapie et flexible pour l’alimentation d’eau pour installation à faux plafond. Ciel de pluie deux jets: jet pluie et nébulise. A installer avec un mitigeur avec inverseur 2 voies.', '6000', 'douches', 'assets/img/douches/dream_flat.jpg', 5, 1),
(27, 'Titre', 'Texte bidon', '560', 'lavabos', 'assets/img/lavabos/1581104597moi.jpg', 100, 0);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `adresse` varchar(255) NOT NULL,
  `politique` varchar(255) NOT NULL,
  `admin` int(1) DEFAULT NULL,
  `moderateur` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `nom`, `prenom`, `email`, `password`, `adresse`, `politique`, `admin`, `moderateur`) VALUES
(3, 'Rouyer', 'Fanny', 'faanny.rouyer@free.fr', '63a9f0ea7bb98050796b649e85481845', '54 RUE DE MONS', 'accepted', 0, 0),
(4, 'Roger', 'Guillaume', 'test@gmail.com', '721a9b52bfceacc503c056e3b9b93cfa', 'blablabla', 'accepted', 0, 0),
(5, 'COUCOU', 'Fanny', 'fy.rouyer@gmail.com', '63a9f0ea7bb98050796b649e85481845', '58 rue des chats', 'accepted', 1, 1),
(6, 'Auchan', 'Lili', 'a@a.fr', '0cc175b9c0f1b6a831c399e269772661', '89 rue du a, Valenciennes 59300', 'accepted', 0, 1),
(7, 'Roger', 'Guillaume', 'b@b.fr', '92eb5ffee6ae2fec3ad71c777531578f', '151 Rue de la Fraternité', 'accepted', 0, 0),
(8, 'Rouyer', 'Fanny', 'c@c.fr', '4a8a08f09d37b73795649038408b5f33', '54 RUE DE MONS', 'accepted', 0, 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
