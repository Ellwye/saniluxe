<?php
    class Utilisateur {
        public $id;
        public $moderateur;


        public function __construct($id = "", $moderateur = "0"){
            $this->id = $id;
            $this->moderateur = $moderateur;
        }

        public function listeUtilisateurs(){
            $BDD = new BDD();
            $dbh = $BDD->getConnection();
            $response = $dbh->prepare("SELECT * FROM user");
            $response->execute();
            return $response->fetchAll();
        }

        public function createUser($nom, $prenom, $email, $password_encrypted, $adresse, $politique, $moderateur){
            $bd = new BDD();
            $co = $bd->getConnection();
            $add = $co->prepare("INSERT INTO user(`nom`, `prenom`, `email`, `password`, `adresse`, `politique`, `moderateur`) VALUES (:nom, :prenom, :email, :pass, :adresse, :politique, :moderateur)");
            $add->bindParam(":nom", $nom);
            $add->bindParam(":prenom", $prenom);
            $add->bindParam(":email", $email);
            $add->bindParam(":pass", $password_encrypted);
            $add->bindParam(":adresse", $adresse);
            $add->bindParam(":politique", $politique);
            $add->bindParam(":moderateur", $moderateur);
            return $add->execute();
        }

        public function verifyUser($email) {
            $co_bdd = new BDD();
            $verif = $co_bdd->getConnection();
            $verif_user = $verif->query("SELECT * FROM user WHERE email = '$email' ");
            return $verif_user;
        }

        public function recupUser($user_id){
            $base = new BDD();
            $bd = $base->getConnection();
            $response_user = $bd->query("SELECT * FROM user WHERE id = '$user_id' ");
            return $response_user->fetch();
        }

        public function majUser($nom, $prenom, $email, $adresse, $moderateur, $password, $user_id){
            $maj = new BDD();
            $maj_user = $maj->getConnection();
            $user_maj = $maj_user->prepare("UPDATE `user` SET `nom`=:nom,`prenom`=:prenom,`email`=:email,`adresse`=:adresse,`moderateur`=:moderateur, `password`=:password WHERE `id`='$user_id'");
            $user_maj->bindParam(':nom',$nom);
            $user_maj->bindParam(':prenom',$prenom);
            $user_maj->bindParam(':email',$email);
            $user_maj->bindParam(':adresse',$adresse);
            $user_maj->bindParam(':moderateur',$moderateur);
            $user_maj->bindParam(':password',$password);
            return $user_maj->execute();
        }

        public function supprUser($user_id){
            $co_suppr = new BDD();
            $suppr_user = $co_suppr->getConnection();
            $suppr = $suppr_user->prepare("DELETE FROM user WHERE id = '$user_id'");
            return $suppr->execute();
        }

        public function get_products_card(){
          $base = new BDD();
          $bd = $base->getConnection();
          $response_user = $bd->query("SELECT SUM(quantity) as total FROM panier WHERE id_user = '$this->id' ");
          return $response_user->fetch();
        }

        public function update_products_quantity(){
          $base = new BDD();
          $bd = $base->getConnection();
          $response_user = $bd->query("SELECT quantity, id_product FROM panier WHERE id_user = '$this->id' ");
          return $response_user->fetchAll();
        }
    }
?>
