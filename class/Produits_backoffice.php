<?php
    class Produits_backoffice {
        public $id_produit;

        public function __construct($id_produit){
            $this->id_produit = $id_produit;
        }

        public function listeProduits(){
            $BDD = new BDD();
            $dbh = $BDD->getConnection();
            $response = $dbh->prepare("SELECT * FROM produits");
            $response->execute();
            return $response->fetchAll();
        }

        public function produitChoisi($id_produit_choisi) {
            $hgf = new BDD();
            $fdl = $hgf->getConnection();
            $prod_choisi = $fdl->prepare("SELECT * FROM produits WHERE id = '$id_produit_choisi'");
            $prod_choisi->execute();
            return $prod_choisi->fetch();
        }

        public function cacher_article($id_produit_cacher){
            $co_suppr = new BDD();
            $suppr_produit = $co_suppr->getConnection();
            $suppr = $suppr_produit->prepare("UPDATE `produits` SET `visible`=:v WHERE id = '$id_produit_cacher'");
            $suppr->bindParam(':v', boolval(0),PDO::PARAM_BOOL);
            return $suppr->execute();
        }

        public function visible_article($id_produit_visible){
            $co_visible = new BDD();
            $visibile_produit = $co_visible->getConnection();
            $visible = $visibile_produit->prepare("UPDATE `produits` SET `visible`=:v WHERE id = '$id_produit_visible'");
            $visible->bindParam(':v', boolval(1),PDO::PARAM_BOOL);
            return $visible->execute();
        }

        public function addArticle($titre, $description, $categorie, $prix, $stock, $image, $visible){
            $bddh = new BDD();
            $bdd_art = $bddh->getConnection();
            $add_article = $bdd_art->prepare("INSERT INTO produits(titre, description, prix, categorie, image, stock, visible) VALUES (:t, :d, :p, :c, :i, :s, :v)");
            $add_article->bindParam(':t',$titre);
            $add_article->bindParam(':d',$description);
            $add_article->bindParam(':c',$categorie);
            $add_article->bindParam(':p',$prix);
            $add_article->bindParam(':s',$stock);
            $add_article->bindParam(':i',$image);
            $add_article->bindParam(':v',boolval($visible),PDO::PARAM_BOOL);
            return $add_article->execute();
        }

        public function majProduit($titre, $description, $categorie, $prix, $stock, $visible, $image, $produit_id){
            $maj = new BDD();
            $maj_produit = $maj->getConnection();
            $produit_maj = $maj_produit->prepare("UPDATE `produits` SET `titre`=:titre,`description`=:description,`categorie`=:categorie,`prix`=:prix,`stock`=:stock, `visible`=:visible,`image`=:image WHERE `id`='$produit_id'");
            $produit_maj->bindParam(':titre',$titre);
            $produit_maj->bindParam(':description',$description);
            $produit_maj->bindParam(':categorie',$categorie);
            $produit_maj->bindParam(':prix',$prix);
            $produit_maj->bindParam(':stock',$stock);
            $produit_maj->bindParam(':visible',$visible);
            $produit_maj->bindParam(':image',$image);
            return $produit_maj->execute();
            // $produit_maj->debugDumpParams();
        }
    }
?>