<?php

class Collection {
    public $id;
    public $titre;
    public $description;
    public $prix;
    public $categorie;
    public $sous_categorie;
    public $image;
    public $stock;

    public function produits_collection(){
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $response = $dbh->prepare("SELECT * FROM produits WHERE stock != '0' && visible != '0'");
        $response->execute();
        $result = $response->fetchAll();
        return $result;
    }

    public function produits_collection_categorie($page_categorie){
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $response = $dbh->prepare("SELECT * FROM produits WHERE categorie = '$page_categorie' && stock != '0' && visible != '0'");
        $response->execute();
        $result = $response->fetchAll();
        return $result;
    }
}
?>