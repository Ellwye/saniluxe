<?php

class Produit {
    public $id;
    public $titre;
    public $description;
    public $prix;
    public $categorie;
    public $sous_categorie;
    public $image;
    public $stock;

    public function __construct($id){
        $this->id = $id;
    }

    public function recherche_produit_by_id(){
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $response = $dbh->prepare("SELECT * FROM produits WHERE id = '$this->id'");
        $response->execute();
        $result = $response->fetch();
        return $result;
    }
}
?>
