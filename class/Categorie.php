<?php
    class Categorie {
        public $id_categorie;

        public function __construct($id_categorie){
            $this->id_categorie = $id_categorie;
        }

        public function listeCategorie(){
            $BDD = new BDD();
            $dbh = $BDD->getConnection();
            $response = $dbh->prepare("SELECT * FROM categories");
            $response->execute();
            return $response->fetchAll();
        }

        public function selectProduit($id_cat){
            $connexion = new BDD();
            $research = $connexion->getConnection();
            $selection = $research->prepare("SELECT produits.id FROM `produits` INNER JOIN `categories` ON (categories.name = produits.categorie) WHERE (categories.id = '$id_cat')");
            $selection->execute();
            return $selection->fetchAll();
        }

        public function modifCategorie($new_name, $id_cat, $response_modif_cat){
            $thf = new BDD();
            $hbd = $thf->getConnection();
            $mofid_cat = $hbd->prepare("UPDATE `categories` SET `name`= '$new_name' WHERE id = '$id_cat'");
            $response_update = $hbd->prepare("UPDATE `produits` SET `categorie`= '$new_name' WHERE id = '$response_modif_cat'");
            $response_update->execute();
            $mofid_cat->execute();
            return true;
        }

        public function supprCategorie($id_cat){
            $bddd = new BDD();
            $hbdd = $bddd->getConnection();
            $suppr_cat = $hbdd->prepare("DELETE FROM categories WHERE id = '$id_cat'");
            $suppr_cat->execute();
            return true;
        }

        public function majCat($new_name, $response_modif_cat){
            $bdddd = new BDD();
            $hbddd = $bdddd->getConnection();
            $modif_cat_suppr = $hbddd->prepare("UPDATE `produits` SET `categorie`= '$new_name' WHERE id = '$response_modif_cat'");
            $modif_cat_suppr->execute();
            return true;
        }

        public function addCategorie($cat_name) {
            $cobdd = new BDD();
            $cb = $cobdd->getConnection();
            $add_cat = $cb->prepare("INSERT INTO categories(`name`) VALUES (:n)");
            $add_cat->bindParam(":n", $cat_name);
            return $add_cat->execute();
        }

        public function verifyCategorie($cat_name) {
            $co_bdd = new BDD();
            $verif = $co_bdd->getConnection();
            $verif_cat = $verif->query("SELECT * FROM categories WHERE name = '$cat_name' ");
            return $verif_cat;
        }

        public function selectCategorie($cat_id){
            $co_sc = new BDD();
            $verifsc = $co_sc->getConnection();
            $verif_sccat = $verifsc->query("SELECT `name` FROM categories WHERE id = '$cat_id' ");
            return $verif_sccat->fetch();
        }
    }
?>