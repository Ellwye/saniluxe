<?php

class Nouveautes {
    public $id;
    public $titre;
    public $description;
    public $prix;
    public $categorie;
    public $sous_categorie;
    public $image;
    public $stock;

    public function produits_en_stock() {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $response = $dbh->prepare("SELECT * FROM produits WHERE stock != '0' && visible != '0'");
        $response->execute();
        $result = $response->fetchAll();
        return $result;
    }
}
?>
