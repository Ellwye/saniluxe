<?php
    class Panier {
        public $user_id;
        public $product_id;
        public $products_in_panier;
        public $number_of_articles;
        public $price_total;
        public $quantity;

        public function __construct($user_id) {
            $this->user_id = $user_id;
        }
        public function save($product_id){
            $dbh = new BDD();
            $bdd = $dbh->getConnection();

            $quantity_product = $bdd->prepare("SELECT * FROM `panier` WHERE id_user = $this->user_id AND id_product = '$product_id'");
            $quantity_product->execute();


            $article = $quantity_product->fetch();
            $done = false;
            if ($article > 0) {

                $this->quantity = $article['quantity'] +1;

                $upload = $bdd->prepare("UPDATE `panier` SET `quantity`= $this->quantity WHERE (id_user = $this->user_id AND id_product = '$product_id')");
                $done = $upload->execute();

            } else {
                $this->quantity = 1;
                $insert = $bdd->prepare("INSERT INTO `panier`(`id_user`, `id_product`, `quantity`) VALUES (:u, :p, :q)");
                $insert->bindParam(':u', $this->user_id);
                $insert->bindParam(':p', $product_id);
                $insert->bindParam(':q', $this->quantity);
                $done = $insert->execute();
            }
            return $done;
        }
        public function update_quantity($product_id,$quantity){
            $dbh = new BDD();
            $bdd = $dbh->getConnection();

            $quantity_product = $bdd->prepare("SELECT * FROM `panier` WHERE id_user = $this->user_id AND id_product = '$product_id'");
            $quantity_product->execute();


            $article = $quantity_product->fetch();
            $done = false;
            if ($article > 0) {

                $this->quantity = $article['quantity']+$quantity;
                $upload = $bdd->prepare("UPDATE `panier` SET `quantity`= $this->quantity WHERE (id_user = $this->user_id AND id_product = '$product_id')");
                $done = $upload->execute();

            } else {
                $this->quantity = 1;
                $insert = $bdd->prepare("INSERT INTO `panier`(`id_user`, `id_product`, `quantity`) VALUES (:u, :p, :q)");
                $insert->bindParam(':u', $this->user_id);
                $insert->bindParam(':p', $product_id);
                $insert->bindParam(':q', $quantity);
                $done = $insert->execute();
            }
            return $done;
        }

        public function product(){
            $connexion = new BDD();
            $research = $connexion->getConnection();
            $selection = $research->prepare("SELECT `id`, `titre`, `prix`, `image`, `stock` FROM `produits` INNER JOIN `panier` ON (produits.id = panier.id_product) WHERE (panier.id_user = $this->user_id)");
            $selection->execute();
            return $selection->fetchAll();
        }

        public function createProducts($produit) {
                $recherche = new BDD();
                $rech = $recherche->getConnection();
                $number_product = $rech->prepare("SELECT * FROM `panier` WHERE id_product = ".$produit['id']."");
                $number_product->execute();
                return $number_product->fetch();
        }


        public function suppr_article($product_id) {
            $bbd = new BDD();
            $research_suppr = $bbd->getConnection();
            $suppr = $research_suppr->prepare("DELETE FROM panier WHERE (id_product = $product_id)");
            $suppr->execute();
        }
    }
?>
