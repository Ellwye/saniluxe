<?php
	if (!isset($_SESSION['authentification_OK']) || (($_SESSION["admin"] == false) && ($_SESSION["moderateur"] == false))){
		echo "<script type='text/javascript'>document.location.replace('index.php?page=login');</script>";
    }

    if (isset($_GET["message"])){
        echo $_GET["message"];
    }
    
    if (!empty($_POST['modif_produit'])){
        $produit_a_modifier = $_POST['select_produit_modified'];
        echo '<script type="text/javascript">document.location.replace("backoffice.php?page=produit_choisi&&produit='.$produit_a_modifier.'");</script>';
    }

    $message_update = false;
    $info_update = "";

    if (isset($_POST["modif_cat"])) {
        $id_cat = htmlspecialchars($_POST['select_categorie_modified']);
        $new_name = htmlspecialchars($_POST['name_categorie_modified']);

        $modifCategorie = new Categorie("");
        $response_modif_cat = $modifCategorie->selectProduit($id_cat);

        foreach ($response_modif_cat as $response) {
            $updated = $modifCategorie->modifCategorie($new_name, $id_cat, $response["id"]);
        }
        

        if ($updated) {
            $info_update = "La catégorie a bien été modifiée.";
            $message_update = true;
        } else {
            $info_update = "La catégorie n'a pas été modifiée, réessayez plus tard.";
            $message_update = true;
        }
    }

    $message_suppri = false;
    $info_suppr = "";

    if (!empty($_POST["supprimer"])) {

        $id_produit = htmlspecialchars($_POST['select_produit_modified']);

        $suppr_produit = new Produits_backoffice("");
        $supprimed = $suppr_produit->cacher_article($id_produit);

        if ($supprimed) {
            $info_suppr = "Le produit a bien été caché.";
            $message_suppri = true;
        } else {
            $info_suppr = "Le produit n'a pas été caché, réessayez plus tard.";
            $message_suppri = true;
        }
    }


    $message_visible = false;
    $info_visible = "";

    if (!empty($_POST["visible"])) {

        $id_produit = htmlspecialchars($_POST['select_produit_visible']);

        $visible_produit = new Produits_backoffice("");
        $visible = $visible_produit->visible_article($id_produit);

        if ($visible) {
            $info_visible = "Le produit a bien été rendu visible.";
            $message_visible = true;
        } else {
            $info_visible = "Le produit n'a pas été rendu visible, réessayez plus tard.";
            $message_visible = true;
        }
    }


    $message_suppr_cat = false;
    $info_suppr_cat = "";

    if (!empty($_POST["suppr_cat"])){
        $id_cat_suppr = $_POST['select_categorie_suppr'];

        $cat_a_supprimer = new Categorie("");
        $produits_cat_suppr = $cat_a_supprimer->selectProduit($id_cat_suppr);
        $new_name = "sans_categorie";
        
        foreach ($produits_cat_suppr as $response) {
            $modified = $cat_a_supprimer->majCat($new_name, $response["id"]);
        }

        $supprimed_cat = $cat_a_supprimer->supprCategorie($id_cat_suppr);
        
        if ($supprimed_cat) {
            $info_suppr_cat = "La catégorie a bien été supprimée.";
            $message_suppr_cat = true;
        } else {
            $info_suppr_cat = "La catégorie n'a pas été supprimée, réessayez plus tard.";
            $message_suppr_cat = true;
        }
    }

    $message_add_cat = false;
    $info_add_cat = "";

    if (!empty($_POST["add_categorie"])){
        $cat_name = $_POST['categorie_add'];

        $liste_cat = new Categorie("");
        $result_liste_cat = $liste_cat->verifyCategorie($cat_name);

        if ($result_liste_cat->fetchColumn() > 0) {
            $message_add_cat = true;
            $info_add_cat = "Erreur : cette catégorie existe déjà, choisissez un autre nom.";
        } else {
            $added = $liste_cat->addCategorie($cat_name);

            if ($added) {
                $message_add_cat = true;
                $info_add_cat = "La catégorie à bien été ajoutée.";
            } else {
                $message_add_cat = true;
                $info_add_cat = "Erreur : la catégorie n'a pas été ajoutée, réessayez plus tard.";
            }
        }
    }
    

    $message_add_produit = false;
    $info_add_produit = "";

// Ajouter un produit
if (!empty($_POST['add_produit'])) {
    $add_titre = htmlspecialchars($_POST['title_produit_add']);
    $add_description = htmlspecialchars($_POST["description_produit_add"]);
    $add_categorie = htmlspecialchars($_POST["select_categorie_add"]);
    $add_prix = htmlspecialchars($_POST["prix_produit_add"]);
    $add_stock = htmlspecialchars($_POST["stock_produit_add"]);

    if (isset($_POST['visible_article'])){
        $add_visible = htmlspecialchars($_POST['visible_article']);
    } else {
        $add_visible = 0;
    }
    

    if(isset($_FILES["photo_produit_add"]) && $_FILES["photo_produit_add"]["error"] == 0){

        $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
        $filename = $_FILES["photo_produit_add"]["name"];
        $filetype = $_FILES["photo_produit_add"]["type"];
        $filesize = $_FILES["photo_produit_add"]["size"];

        // Vérifie l'extension du fichier
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!array_key_exists($ext, $allowed)) {
            $message_add_produit = true;
            $info_add_produit = "Erreur : Veuillez sélectionner un format de fichier valide.";
        } else {
            // Vérifie la taille du fichier - 5Mo maximum
            $maxsize = 5 * 1024 * 1024;
            if($filesize > $maxsize) {
                $message_add_produit = true;
                $info_add_produit = "Erreur : La taille du fichier est supérieure à la limite autorisée.";
            } else {
                // Vérifie le type MIME du fichier
                if(in_array($filetype, $allowed)){
                    $cat_name_add = new Categorie("");
                    $result_cat_name = $cat_name_add->selectCategorie($add_categorie);
                    // Vérifie si le fichier existe avant de le télécharger.
                    move_uploaded_file($_FILES["photo_produit_add"]["tmp_name"], "assets/img/".$result_cat_name['name']."/" . time().$_FILES["photo_produit_add"]["name"]);

                    $add_categorie = $result_cat_name['name'];
                    $add_image = "assets/img/".$add_categorie."/" . time().$_FILES["photo_produit_add"]["name"];

                } else{
                    $message_add_produit = true;
                    $info_add_produit = "Erreur : Il y a eu un problème de téléchargement de votre fichier. Veuillez réessayer.";
                }
            }
        }
    }

    // Requette pour envoyer les données dans la BDD
    $requette_add = new Produits_backoffice("");
    $inserted_add = $requette_add->addArticle($add_titre, $add_description, $add_categorie, $add_prix, $add_stock, $add_image, $add_visible);

    if ($inserted_add) {
        $message_add_produit = true;
        $info_add_produit = "Le produit a bien été ajouté.";
    } else {
        $message_add_produit = true;
        $info_add_produit = "Erreur : Il y a eu un problème. Veuillez réessayer.";
    }

}
?>
<section class="back_office_container">
    <div class="container">
        <div class="row justify-content-between padding-bottom align-items-start">
            <!-- Partie 1 : catégories -->
            <div class="col-lg-6 col-md-6 col-sm-12" id="categories-bo">

                <!-- Modifier une catégorie -->
                <div class="formulaire">
                    <form method="POST" action="backoffice.php?page=produits-back-office">
                        <h2>Modifier une catégorie</h2>
                        <div class="form-group">
                            <label for="select_categorie_modified">Choisir la catégorie à modifier</label>
                            <select name="select_categorie_modified" class="form-control" required>
                                <option value="">Catégorie</option>

                                <?php
                                    $cat = new Categorie("");
                                    $result = $cat->listeCategorie();

                                    foreach ($result as $categorie) {
                                        echo '
                                            <option value="'.$categorie["id"].'">'.$categorie["name"].'</option>
                                        ';
                                    }
                                ?>

                            </select>
                        </div>
                        <div class="form-group">
                            <label for="name_categorie_modified" required>Modifier la catégorie</label>
                            <input type="text" class="form-control" name="name_categorie_modified" required>
                        </div>
                        <button type="submit" class="btn btn-primary button-backoffice" name="modif_cat">Modifier la catégorie</button>
                    </form>
                    <?php
                        if ($message_update) {
                            echo "<p class='info-create-user'>$info_update</p>";
                        }
                    ?>
                </div>

                <!-- Ajouter une catégorie -->
                <div class="formulaire">
                    <form method="POST" action="backoffice.php?page=produits-back-office">
                        <h2>Ajouter une catégorie</h2>
                        <div class="form-group">
                            <label for="categorie_add">Nom de la catégorie</label>
                            <input type="text" class="form-control" name="categorie_add">
                        </div>
                        <button type="submit" class="btn btn-primary button-backoffice" name="add_categorie" value="add">Ajouter la catégorie</button>
                    </form>
                    <?php
                        if ($message_add_cat) {
                            echo "<p class='info-create-user'>$info_add_cat</p>";
                        }
                    ?>
                </div>

                <!-- Supprimer une catégorie -->
                <div class="formulaire">
                    <form method="POST" action="backoffice.php?page=produits-back-office">
                        <h2>Supprimer une catégorie</h2>
                        <div class="form-group">
                            <label for="select_categorie_suppr">Choisir la catégorie à supprimer</label>
                            <select name="select_categorie_suppr" class="form-control" required>
                                <option value="">Catégorie</option>

                                <?php
                                    $cat = new Categorie("");
                                    $result = $cat->listeCategorie();

                                    foreach ($result as $categorie) {
                                        echo '
                                            <option value="'.$categorie["id"].'">'.$categorie["name"].'</option>
                                        ';
                                    }
                                ?>

                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary button-backoffice" name="suppr_cat" value="supprimer">Supprimer la catégorie</button>
                    </form>
                    <?php
                        if ($message_suppr_cat) {
                            echo "<p class='info-create-user'>$info_suppr_cat</p>";
                        }
                    ?>
                </div>
            </div>
            
            <!-- Partie 2 : produits -->
            <div class="col-lg-6 col-md-6 col-sm-12" id="produits-bo">

                <!-- Modifier un produit -->
                <div class="formulaire">
                    <form method="POST" action="backoffice.php?page=produits-back-office">
                        <h2>Modifier un produit</h2>
                        <div class="form-group">
                            <label for="select_produit_modified">Choisir le produit à modifier</label>
                            <select name="select_produit_modified" class="form-control" required>
                                <option selected>Produit</option>
                                <?php
                                    $produit = new Produits_backoffice("");
                                    $result = $produit->listeProduits();

                                    foreach ($result as $produit) {
                                        echo '
                                            <option value="'.$produit["id"].'">'.$produit["titre"].'</option>
                                        ';
                                    }
                                ?>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary button-backoffice" name="modif_produit" value="modif">Modifier le produit</button>
                    </form>
                </div>

                <!-- Ajouter un produit -->
                <div class="formulaire">
                    <form  enctype="multipart/form-data" method="POST" action="backoffice.php?page=produits-back-office">
                        <h2>Ajouter un produit</h2>
                        <div class="form-group">
                            <label for="title_produit_add">Titre du produit</label>
                            <input type="text" class="form-control" name="title_produit_add" required>
                        </div>
                        <div class="form-group">
                            <label for="description_produit_add">Description du produit</label>
                            <textarea class="form-control" name="description_produit_add" rows="5" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="select_categorie_add">Choisir une catégorie</label>
                            <select name="select_categorie_add" class="form-control" required>
                                <option value="">Catégorie</option>
                                <?php
                                    $cat_add_produit = new Categorie("");
                                    $result_add_cat_produit = $cat_add_produit->listeCategorie();

                                    foreach ($result_add_cat_produit as $categorie) {
                                        echo '
                                            <option value="'.$categorie["id"].'">'.$categorie["name"].'</option>
                                        ';
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="prix_produit_add">Prix du produit (en euros)</label>
                            <input type="number" class="form-control" name="prix_produit_add" required>
                        </div>
                        <div class="form-group">
                            <label for="stock_produit_add">Stock du produit</label>
                            <input type="number" class="form-control" name="stock_produit_add" required>
                        </div>
                        <div class="form-group">
                            <label for="photo_produit_add">Choisir une photo du produit</label>
                            <input type="file" class="form-control-file" name="photo_produit_add" required>
                            <p><br><strong>Note:</strong> Seuls les formats .jpg, .jpeg, .jpeg, .gif, .png sont autorisés jusqu'à une taille maximale de 5 Mo.</p>
                        </div>
                        <div class="form-group">
                            <input type="checkbox" class="custom-control-input" id="customControlAutosizing" name="visible_article" value="1">
                            <label class="custom-control-label" for="customControlAutosizing">Rendre l'article visible</label>
                        </div>
                        <button type="submit" class="btn btn-primary button-backoffice" name="add_produit" value="add">Ajouter le produit</button>
                    </form>
                    <?php
                        if ($message_add_produit) {
                            echo "<p class='info-create-user'>$info_add_produit</p>";
                        }
                    ?>
                </div>

                <!-- Cacher un produit -->
                <div class="formulaire">
                    <form method="POST" action="backoffice.php?page=produits-back-office">
                        <h2>Cacher un produit pour les clients</h2>
                        <div class="form-group">
                            <label for="select_produit_modified">Choisir le produit à cacher</label>
                            <select name="select_produit_modified" class="form-control" required>
                                <option value="">Produit</option>

                                <?php
                                    foreach ($result as $produit) {
                                        if ($produit["visible"] == 1) {
                                            echo '
                                                <option value="'.$produit["id"].'">'.$produit["titre"].'</option>
                                            ';
                                        }
                                    }
                                ?>

                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary button-backoffice" name="supprimer" value="suppr">Cacher le produit</button>
                    </form>
                    <?php
                        if ($message_suppri) {
                            echo "<p class='info-create-user'>$info_suppr</p>";
                        }
                    ?>
                </div>

                <!-- Rendre visible un produit -->
                <div class="formulaire">
                    <form method="POST" action="backoffice.php?page=produits-back-office">
                        <h2>Rendre visible un produit pour les clients</h2>
                        <div class="form-group">
                            <label for="select_produit_visible">Choisir le produit à rendre visible</label>
                            <select name="select_produit_visible" class="form-control" required>
                                <option value="">Produit</option>

                                <?php
                                    foreach ($result as $produit) {
                                        if ($produit["visible"] == 0) {
                                            echo '
                                                <option value="'.$produit["id"].'">'.$produit["titre"].'</option>
                                            ';
                                        }
                                    }
                                ?>

                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary button-backoffice" name="visible" value="visible">Rendre visible le produit</button>
                    </form>
                    <?php
                        if ($message_visible) {
                            echo "<p class='info-create-user'>$info_visible</p>";
                        }
                    ?>
                </div>

            </div>
        </div>
    </div>
</section>