<?php
	if (!isset($_SESSION['authentification_OK']) || (($_SESSION["admin"] == false) && ($_SESSION["moderateur"] == false))){
		echo "<script type='text/javascript'>document.location.replace('index.php?page=login');</script>";
	}

	if (!empty($_POST["select_user"])) {
		$user = $_POST["select_user"];
		echo '<script type="text/javascript">document.location.replace("backoffice.php?page=utilisateur-choisi&&user='.$user.'");</script>';
	}



	$mess = false;
	$message = "";

	if (!empty($_POST['nom']) && !empty($_POST['prenom']) && !empty($_POST['email']) && !empty($_POST['adresse']) && ($_POST['password1']) && ($_POST['password2'])) {
		$nom = htmlspecialchars($_POST['nom']);
		$prenom = htmlspecialchars($_POST['prenom']);
		$email = htmlspecialchars($_POST['email']);
		$adresse = htmlspecialchars($_POST['adresse']);
		$password1 = htmlspecialchars($_POST['password1']);
		$password2 = htmlspecialchars($_POST['password2']);
		$politique = "accepted";
		// echo "$nom<br> $prenom<br> $email<br> $adresse<br> $password1<br> $password2<br> $politique<br>";

		$user_unique = new Utilisateur();
		$response_register = $user_unique->verifyUser($email);

        if($response_register->fetchColumn() > 0){
            $mess = true;
            $message = "Erreur : ce login est déjà utilisé";
        } else {

			if ($password1 == $password2) {
				$password_encrypted = md5($password1);
	
				if (isset($_POST["moderateur"])){
					$moderateur = 1;
					// echo "Moderateur ? : " . $moderateur;
	
					// ajouter l'utilisateur à la bdd avec moderateur = 1
					$new_user = new Utilisateur();
					$inserted = $new_user->createUser($nom, $prenom, $email, $password_encrypted, $adresse, $politique, $moderateur);
	
					if ($inserted) {
						$mess = true;
						$message = "L'utilisateur a bien été créé.";
					} else {
						$mess = true;
						$message = "Erreur : l'utilisateur n'a pas été créé, réessayez.";
					}
				} else {
					$moderateur = 0;
					// ajouter l'utilisateur a la bdd avec moderateur = 0
					$new_user = new Utilisateur();
					$inserted = $new_user->createUser($nom, $prenom, $email, $password_encrypted, $adresse, $politique, $moderateur);
					echo $inserted;
	
					if ($inserted) {
						$mess = true;
						$message = "L'utilisateur a bien été créé.";
					} else {
						$mess = true;
						$message = "Erreur : l'utilisateur n'a pas été créé, réessayez.";
					}
				}
			} else {
				$mess = true;
				$message = "Erreur : les mots de passes ne sont pas identiques.";
			}

		}

		
	}
?>

<section class="back_office_container">
	<div class="container">
		<div class="row justify-content-between padding-bottom align-items-start">
			<div class="col-lg-6 col-md-12 col-sm-12">
			<div class="formulaire">
				<form method="POST" action="backoffice.php?page=utilisateurs-back-office">
					<h2>Voir les informations d'un utilisateur</h2>
					<div class="form-group">
						<label for="select_user">Voir les informations d'un utilisateur</label>
						<select name="select_user" class="form-control">
							<option value="">Choisissez un utilisateur</option>
							<?php
								$user = new Utilisateur();
								$result = $user->listeUtilisateurs();

								foreach ($result as $utilisateur) {
									echo '
										<option value="'.$utilisateur["id"].'">'.$utilisateur["email"].'</option>
									';
								}
							?>
						</select>
					</div>
					<button type="submit" class="btn btn-primary button-backoffice">Voir les informations de l'utilisateur</button>
				</form>
			</div>
			</div>
			<div class="col-lg-6 col-md-12 col-sm-12">
				<div class="formulaire">
					<form method="POST" action="backoffice.php?page=utilisateurs-back-office">
						<h2>Ajouter un utilisateur</h2>
						<div class="form-group">
							<label for="nom">Nom</label>
							<input type="text" class="form-control" name="nom" required>
						</div>
						<div class="form-group">
							<label for="nom">Prénom</label>
							<input type="text" class="form-control" name="prenom" required>
						</div>
						<div class="form-group">
							<label for="nom">Email</label>
							<input type="email" class="form-control" name="email" required>
						</div>
						<div class="form-group">
							<label for="nom">Adresse</label>
							<input type="text" class="form-control" name="adresse" required>
						</div>
						<div class="form-group">
							<label for="nom">Mot de passe</label>
							<input type="password" class="form-control" name="password1" required>
						</div>
						<div class="form-group">
							<label for="nom">Vérifiez le mot de passe</label>
							<input type="password" class="form-control" name="password2" required>
						</div>

						<?php
						if ($_SESSION["admin"] == true) {
						echo '
						<div class="form-group">
							<input type="checkbox" class="custom-control-input" id="customControlAutosizing" name="moderateur" value="1">
							<label class="custom-control-label" for="customControlAutosizing">Cet utilisateur est modérateur</label>
						</div>
						';
						}
						?>
						<button type="submit" class="btn btn-primary button-backoffice">Créer un nouvel utilisateur</button>
					</form>

					<?php
						if ($mess) {
							echo "<p class='info-create-user'>$message</p>";
						}
					?>
				</div>
			</div>
		</div>
	</div>
</section>