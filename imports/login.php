<?php

$dbh = new BDD();
$bdd = $dbh->getConnection();

if(isset($_SESSION['authentification_OK']) ) {
    echo "<script type='text/javascript'>document.location.replace('index.php?page=user');</script>";
}

// si il y a une erreur, alors on affichera un message pour l'indiquer à l'utilisateur
$error_login = false;
$message_login = "";
$error_register = false;
$message_register = "";


/* ----- CONNEXION ----- */
$email_login = null;
$password_login = null;

if( !empty($_POST['email_login']) && !empty($_POST['password_login']) ){
    $email_login  = htmlspecialchars($_POST['email_login']);
    $password_login = htmlspecialchars($_POST['password_login']);
    $cryptage_mdp_login = md5($password_login);

    // Vérification si un utiliseur corresponds au couple login / mot de passe
    $response = $bdd->query("SELECT * FROM user WHERE email = '$email_login' AND password = '$cryptage_mdp_login'");
    //on ne peut récupérer que 0 ou 1 résultat, car il ne peut y avoir au maximum qu'un seul utilisateur avec un couple login mot de passe, donc on essaye de récupérer la 1ère ligne de resultat
    $user = $response->fetch();

    if ($user) {
        // l'utilisateur a bien rentré le bon login / mot de passe, on va donc créer une SESSION afin de l'autoriser à accéder à toutes les pages protégées sans qu'il n'ai besoin de rentrer son login / mot de passe
        $_SESSION['authentification_OK'] = true;
        // on va également garder en mémoire son login afin de pouvoir l'identifier sur toutes les pages
        $_SESSION['email'] = $user['email'];
        // on va également garder en mémoire son identifiant
        $_SESSION['id'] = $user['id'];

        if ($user["admin"] == 1) {
            $_SESSION['admin'] = true;
        } else {
            $_SESSION['admin'] = false;
        }

        if ($user["moderateur"] == 1) {
            $_SESSION['moderateur'] = true;
        } else {
            $_SESSION['moderateur'] = false;
        }

        // on le redirige vers sa page de profil
        echo "<script type='text/javascript'>document.location.replace('index.php?page=user');</script>";
    } else {
        $error_login = true;
        $message_login = "Login ou mot de passe incorrect";
    }

    //fermeture requete
    $response->closeCursor();
}


/* ----- INSCRIPTION ----- */
if (isset($_POST['register'])) {
    $nom_register = null;
    $prenom_register = null;
    $email_register = null;
    $password1 = null;
    $password2 = null;
    $adresse_register = null;
    $politique_register = null;
    $inserted = "";

    if (isset($_POST["nom"]) && isset($_POST["prenom"]) && isset($_POST["email"]) && isset($_POST["password1"]) && isset($_POST["password2"]) && isset($_POST["adresse"]) && isset($_POST["politique"])) {
        $nom_register = htmlspecialchars($_POST["nom"]);
        $prenom_register = htmlspecialchars($_POST["prenom"]);
        $email_register = htmlspecialchars($_POST["email"]);
        $password1 = htmlspecialchars($_POST["password1"]);
        $password2 = htmlspecialchars($_POST["password2"]);
        $adresse_register = htmlspecialchars($_POST["adresse"]);
        $politique_register = htmlspecialchars($_POST["politique"]);


        $response_register = $bdd->query("SELECT * FROM user WHERE email = '$email_register' ");
        if($response_register->fetchColumn() > 0){
            $error_register = true;
            $message_register = "Erreur : ce login est déjà utilisé";
        } else {
            if ($password1 == $password2) {
                $password_encrypted = md5($password1);

                $envoi_bdd = $bdd->prepare("INSERT INTO user (nom, prenom, email, password, adresse, politique) VALUES (:n, :pr, :e, :pa, :a, :po)");
                $envoi_bdd->bindParam(":n", $nom_register);
                $envoi_bdd->bindParam(":pr", $prenom_register);
                $envoi_bdd->bindParam(":e", $email_register);
                $envoi_bdd->bindParam(":pa", $password_encrypted);
                $envoi_bdd->bindParam(":a", $adresse_register);
                $envoi_bdd->bindParam(":po", $politique_register);
                $inserted = $envoi_bdd->execute();

                $error_register = true;
                $message_register = "L'inscription a bien été prise en compte, vous pouvez vous connecter.";

                $transport = (new Swift_SmtpTransport('smtp.mailtrap.io', 25))
                  ->setUsername('d434bdda646de8')
                  ->setPassword('af8ce19cff89cf')
                ;
                $mailer = new Swift_Mailer($transport);
                $message = (new Swift_Message('Inscription au site Sanilux'))
                ->setFrom(['faanny.rouyer@gmail.com' => 'Sanilux Team'])
                ->setTo([$email_register=>$nom_register.' '.$prenom_register])
                ->setBody('Bienvenue sur le site sanilux.fr')
                ->addPart('<p>Bienvenue sur le super site <a style="color:red">sanilux.fr</a></p><br />
                Enjoy', 'text/html');

                // Send the message
                $result = $mailer->send($message);

            } else {
                $error_register = true;
                $message_register = "Erreur : les mots de passes ne sont pas identiques";
            }
        }
    } else {
        $error_register = true;
        $message_register = "Erreur : remplissez tous les champs";
    }
}

?>

<section id="login_register">
    <div class="container">
        <div class="log_reg_alignement">
            <!-- Login -->
            <div class="form-style">
                <h2>Connexion</h2>
                <p class="subtitle">
                    Déjà client ? Connectez-vous.
                </p>
                <div class="background">
                    <form action="index.php?page=login" method="POST" id="login">
                        <input type="email" name="email_login" placeholder="Email" required>
                        <input type="password" name="password_login" placeholder="Mot de passe" required>
                        <button type="submit" class="buttons buttons-style" name="login">Connexion</button>
                    </form>
                    <div class="message_info">
                        <p><?php if ($error_login) {
                            echo $message_login;
                        } ?></p>
                    </div>
                </div>
            </div>

            <!-- Register -->
            <div class="form-style">
                <h2>Inscription</h2>
                <p class="subtitle">
                    Nouveau client ? Inscrivez-vous.
                </p>
                <div class="background">
                    <form action="index.php?page=login" method="POST">
                        <input type="text" name="nom" placeholder="Nom" required>
                        <input type="text" name="prenom" placeholder="Prénom" required>
                        <input type="email" name="email" placeholder="Email" required>
                        <input type="password" name="password1" placeholder="Choisissez un mot de passe" required>
                        <input type="password" name="password2" placeholder="Vérifier votre mot de passe" required>
                        <input type="text" name="adresse" placeholder="Adresse" required>
                        <div class="checkbox">
                            <input type="checkbox" name="politique" value="accepted" required>
                            <p>J'accepte la <a href="index.php?page=politique">politique de confidentialité</a> du site</p>
                        </div>
                        <button type="submit" class="buttons buttons-style" name="register">Inscription</button>
                    </form>
                    <div class="message_info">
                        <p><?php if ($error_register) {
                            echo "$message_register";
                        } ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
