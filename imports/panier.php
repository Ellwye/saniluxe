<?php

    if(!isset($_SESSION['authentification_OK'])){
        echo "<script type='text/javascript'>document.location.replace('index.php?page=login');</script>";
    }
    
    if (!empty($_POST['cart_add'])) {
        $add_panier = new Panier($_SESSION["id"]);
        $add_panier->save($_POST['cart_add']);
    }

    if (!empty($_POST['suppr'])) {
        $suppr_article = new Panier($_SESSION["id"]);
        $suppr_article->suppr_article($_POST['suppr']);
    }
?>

<section id="panier">
    <div class="container">
        <h2>
            Mon panier
        </h2>

        <div class="align-sections">
            <section id="produits">

            <?php
                $product = new Panier($_SESSION["id"]);
                $products_in_panier = $product->product();

                $number_of_articles = 0;
                $price_total = 0;

                foreach ($products_in_panier as $produit) {
                    $numb = $product->createProducts($produit);

                    echo '
                    <div class="produit">
                        <h5>'.$produit["titre"].'</h5>
                        <h5 class="prix">'.$produit["prix"].'€</h5>
                        <div class="align-img">
                            <div class="img">
                                <img src="'.$produit["image"].'">
                            </div>
                            <div class="align-items">
                                <form action="index.php?page=panier" method="POST">
                                    <select name="quantity">
                                        <option value="'.$numb["quantity"].'">Quantité : '.$numb["quantity"].'</option>

                    ';

                    for($i = 1; $i <= $produit["stock"]; $i++) {
                        if ($i != $numb['quantity']) {
                            $option = '
                                <option value="'.$i.'">Quantité : '.$i.'</option>
                            ';
                            echo $option;
                        }
                    }

                    echo '
                            </select>
                                </form>
                                <form action="index.php?page=panier" method="POST">
                                    <button type="submit" name="suppr" value="'.$produit["id"].'">Supprimer</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    ';

                    $number_of_articles += $numb['quantity'];
                    $price_total += ($produit["prix"] * $numb['quantity']);
                }

            ?>
                <div class="line"></div>
                <p class="total">Sous-total (<?php echo $number_of_articles ?> article(s)) : <?php echo $price_total ?> €</p>
            </section>
    
            <section id="recapitulatif">
                <div class="recap">
                    <p>Sous-total (<?php echo $number_of_articles ?> article(s)) : <span class="text-hilight"><?php echo $price_total ?> €</span></p>
                    <form action="index.php?page=panier" method="POST">
                        <button type="submit" name="commander" value="commander" class="buttons buttons-style">Passer la commande</button>
                    </form>
                </div>
            </section>
        </div>
    </div>
</section>