<div class="back_office_container">
	<div class="container">
		<div class="row menu">
			<ul class="nav nav-pills">
				<li class="nav-item">
					<a class="nav-link <?php 
							if (isset($_GET["page"])) {
								if ($_GET["page"] == "utilisateurs-back-office" || $_GET["page"] == "utilisateur-choisi") {
									echo 'active';
								}
							}?>" href="backoffice.php?page=utilisateurs-back-office">Utilisateurs</a>
				</li>
				<li class="nav-item">
					<a class="nav-link <?php 
						if (isset($_GET["page"])) {
							if ($_GET["page"] == "produits-back-office" || $_GET["page"] == "produit_choisi") {
								echo 'active';
							}
						}?>" href="backoffice.php?page=produits-back-office">Produits</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="index.php?page=user">Retour au profil</a>
				</li>
			</ul>
		</div>
	</div>
</div>