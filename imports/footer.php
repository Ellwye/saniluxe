<!-- Footer -->
<footer>
    <div class="container">
        <div class="align-items">
            <div class="item">
                <img src="assets/img/logotype-sanilux.svg" alt="Logotype sanilux">
                <ul class="rs">
                    <li><a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="https://www.instagram.com/?hl=fr" target="_blank"><i class="fa fa-instagram"></i></a>
                    </li>
                </ul>
            </div>
            <div class="item">
                <ul class="links">
                    <li><a href="index.php?page=politique">Politique de confidentialité</a></li>
                    <li><a href="index.php?page=mentions">Mentions légales</a></li>
                    <li><a href="index.php?page=conditions">Conditions générales de vente</a></li>
                    <li><a href="index.php?page=plan">Plan du site</a></li>
                </ul>
            </div>
            <div class="item">
                <h6>S'abonner à la newsletter</h6>
                <form>
                    <input type="email" placeholder="Votre adresse email">
                    <button type="submit"><i class="fas fa-arrow-right"></i></button>
                </form>
            </div>
        </div>
    </div>

    <!-- Copyright -->
    <div class="copyright">
        <p>© 2020 Tous droits réservés Sanilux</p>
    </div>
    <!-- End Copyright -->
</footer>
<!-- End footer -->

<?php 
    if(isset($_SESSION['$_SESSION["authentification_OK"]'])){
        echo '<div class="modal fade" id="modalProduct" tabindex="-1" role="dialog" aria-labelledby="modalProductLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalProductLabel"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row mb-3">
                        <div class="image-product"></div>
                        <div class="contenu-product">
                            <div class="produit">
                            </div>
                            <div class="actions">
                                <h5>Ajouter au panier</h5>
    
                                <label class="sr-only" for="quantite">Username</label>
                                <div class="input-group mb-2 mr-sm-2">
    
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Quantité</div>
                                    </div>
    
                                    <select name="quantite" id="quantite" class="form-control">
                                    </select>
    
                                </div>
    
                                <button class="buttons buttons-style-button btn-add-to-card" name="cart_add">
                                    <i class="fas fa-cart-plus"></i> Ajouter au panier
                                </button>
                            </div>
    
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="button button-cancel" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>';
    } else {
        echo '<div class="modal fade" id="modalProduct" tabindex="-1" role="dialog" aria-labelledby="modalProductLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalProductLabel"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row mb-3">
                        <div class="image-product"></div>
                        <div class="contenu-product">
                            <div class="produit">
                            </div>
                            <div class="actions">
                                <h5>Ajouter au panier</h5>
                                <p class="text-center">Pour pouvoir ajouter cet article au panier, vous devez vous connecter.</p>

                                <a href="index.php?page=login" class="buttons buttons-style-button">
                                    Se connecter / S\'inscrire
                                </a>
                            </div>
    
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="button button-cancel" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>';
    }
?>