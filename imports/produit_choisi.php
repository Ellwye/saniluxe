<?php
	if (!isset($_SESSION['authentification_OK']) || (($_SESSION["admin"] == false) && ($_SESSION["moderateur"] == false))){
        echo "<script type='text/javascript'>document.location.replace('index.php?page=login');</script>";
    }
    
    $produit_id = $_GET["produit"];
    $produit = new Produits_backoffice("");
    $produit_choisi = $produit->produitChoisi($produit_id);

    $message_upload_produit = false;
    $info_upload_produit = "";

    if (isset($_POST['modif_produit'])) {

        $visible_upload = intval(isset($_POST['visible_article_upload']));

        if (!empty($_POST['title_produit_upload']) && ($_POST['title_produit_upload'] != $produit_choisi["titre"]) || !empty($_POST['description_produit_upload']) && ($_POST['description_produit_upload'] != $produit_choisi["description"]) || !empty($_POST['select_categorie_upload']) && ($_POST['select_categorie_upload'] != $produit_choisi["categorie"]) || !empty($_POST['prix_produit_add']) && ($_POST['prix_produit_add'] != $produit_choisi["prix"]) || !empty($_POST['stock_produit_upload']) && ($_POST['stock_produit_upload'] != $produit_choisi["stock"]) || isset($_FILES["photo_produit_upload"]) && $_FILES["photo_produit_upload"]["error"] == 0 || $visible_upload != $produit_choisi["visible"]) {

            $titre_upload = htmlspecialchars($_POST["title_produit_upload"]);
            $description_upload = htmlspecialchars($_POST["description_produit_upload"]);
            $categorie_upload = htmlspecialchars($_POST["select_categorie_upload"]);
            $prix_upload = htmlspecialchars($_POST["prix_produit_add"]);
            $stock_upload = htmlspecialchars($_POST["stock_produit_upload"]);


            if(isset($_FILES["photo_produit_upload"]) && $_FILES["photo_produit_upload"]["error"] == 0){

                $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
                $filename = $_FILES["photo_produit_upload"]["name"];
                $filetype = $_FILES["photo_produit_upload"]["type"];
                $filesize = $_FILES["photo_produit_upload"]["size"];
        
                // Vérifie l'extension du fichier
                $ext = pathinfo($filename, PATHINFO_EXTENSION);

                if(!array_key_exists($ext, $allowed)) {

                    $message_upload_produit = true;
                    $info_upload_produit = "Erreur : Veuillez sélectionner un format de fichier valide.";

                } else {

                    // Vérifie la taille du fichier - 5Mo maximum
                    $maxsize = 5 * 1024 * 1024;

                    if($filesize > $maxsize) {

                        $message_upload_produit = true;
                        $info_upload_produit = "Erreur : La taille du fichier est supérieure à la limite autorisée.";

                    } else {

                        // Vérifie le type MIME du fichier
                        if(in_array($filetype, $allowed)){

                            $cat_name_add = new Categorie("");

                            if (isset($_POST['select_categorie_upload']) && ($_POST['select_categorie_upload'] != $produit_choisi["categorie"])) {

                                $add_categorie = $_POST['select_categorie_upload'];

                                $add_image = "assets/img/".$add_categorie."/" . time().$_FILES["photo_produit_upload"]["name"];

                                // Vérifie si le fichier existe avant de le télécharger.
                                move_uploaded_file($_FILES["photo_produit_upload"]["tmp_name"], $add_image);
            
                                $inserted = $produit->majProduit($titre_upload, $description_upload, $categorie_upload, $prix_upload, $stock_upload, $visible_upload, $add_image, $produit_id);

                                if ($inserted) {
                                    $message_upload_produit = true;
                                    $info_upload_produit = "Le produit a bien été mis à jour.";
                                } else {
                                    $message_upload_produit = true;
                                    $info_upload_produit = "Le produit n'a pas été mis à jour, réessayez.";
                                }


                            } else {
                                $result_cat_name = $produit_choisi["categorie"];
                                $add_image = "assets/img/".$result_cat_name."/" . time().$_FILES["photo_produit_upload"]["name"];

                                // Vérifie si le fichier existe avant de le télécharger.
                                move_uploaded_file($_FILES["photo_produit_upload"]["tmp_name"], $add_image);
            
                                $inserted = $produit->majProduit($titre_upload, $description_upload, $categorie_upload, $prix_upload, $stock_upload, $visible_upload, $add_image, $produit_id);

                                if ($inserted) {
                                    $message_upload_produit = true;
                                    $info_upload_produit = "Le produit a bien été mis à jour.";
                                } else {
                                    $message_upload_produit = true;
                                    $info_upload_produit = "Le produit n'a pas été mis à jour, réessayez.";
                                }
                            }
                            
        
                        } else{
                            $message_upload_produit = true;
                            $info_upload_produit = "Erreur : Il y a eu un problème de téléchargement de votre fichier. Veuillez réessayer.";
                        }
                    }
                }
            } else {

                $add_image = $produit_choisi["image"];
                
                $inserted = $produit->majProduit($titre_upload, $description_upload, $categorie_upload, $prix_upload, $stock_upload, $visible_upload, $add_image, $produit_id);
    
                if ($inserted) {
                    $message_upload_produit = true;
                    $info_upload_produit = "Le produit a bien été mis à jour.";
                } else {
                    $message_upload_produit = true;
                    $info_upload_produit = "Le produit n'a pas été mis à jour, réessayez.";
                }
                
            }

        }

    }

?>

<section class="back_office_container">
    <div class="container">
        <div class="row justify-content-center padding-bottom align-items-start">
            
            <!-- Modifier un produit -->
            <div class="col-lg-6 col-md-6 col-sm-12" id="produits-bo">

                <!-- Modifier un produit -->
                <div class="formulaire">
                    <form enctype="multipart/form-data" method="POST" action="backoffice.php?page=produit_choisi&&produit=<?php echo $produit_id ?>">
                        <h2>Modifier un produit</h2>
                        <div class="form-group">
                            <label for="title_produit_upload">Titre du produit</label>
                            <input type="text" class="form-control" name="title_produit_upload" value="<?php echo $produit_choisi["titre"] ?>">
                        </div>
                        <div class="form-group">
                            <label for="description_produit_upload">Description du produit</label>
                            <textarea class="form-control" name="description_produit_upload" rows="5" value="<?php echo $produit_choisi["description"] ?>"><?php echo $produit_choisi["description"] ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="select_categorie_upload">Choisir une catégorie</label>
                            <select name="select_categorie_upload" class="form-control">
                                <option value="<?php echo $produit_choisi["categorie"] ?>"><?php echo $produit_choisi["categorie"] ?></option>
                                
                                <?php
                                    $categorie = new Categorie("");
                                    $result = $categorie->listeCategorie();

                                    foreach ($result as $categorie) {

                                        if ($categorie['name'] != $produit_choisi["categorie"]) {
                                            echo '
                                                <option value="'.$categorie["name"].'">'.$categorie["name"].'</option>
                                            ';
                                        }
                                    }
                                ?>

                            </select>
                        </div>
                        <div class="form-group">
                            <label for="prix_produit_add">Prix du produit (en euros)</label>
                            <input type="number" class="form-control" name="prix_produit_add" value="<?php echo $produit_choisi["prix"] ?>">
                        </div>
                        <div class="form-group">
                            <label for="stock_produit_upload">Stock du produit</label>
                            <input type="number" class="form-control" name="stock_produit_upload" value="<?php echo $produit_choisi["stock"] ?>">
                        </div>
                        <div class="form-group">
                            <label for="photo_produit_upload">Choisir une photo du produit</label>
                            <input type="file" class="form-control-file" name="photo_produit_upload">
                            <p><br><strong>Note:</strong> Seuls les formats .jpg, .jpeg, .jpeg, .gif, .png sont autorisés jusqu'à une taille maximale de 5 Mo.</p>
                        </div>
                        <div class="form-group">
                            <input type="checkbox" class="custom-control-input" id="customControlAutosizing" name="visible_article_upload" value="1" <?php if ($produit_choisi['visible'] == 1){echo "checked";} ?>>
                            <label class="custom-control-label" for="customControlAutosizing">Article visible</label>
                        </div>
                        <button type="submit" class="btn btn-primary button-backoffice" name="modif_produit">Modifier le produit</button>
                    </form>
                    <?php
                        if ($message_upload_produit) {
                            echo "<p class='info-create-user'>$info_upload_produit</p>";
                        }
                    ?>
                </div>

            </div>
        </div>
    </div>
</section>