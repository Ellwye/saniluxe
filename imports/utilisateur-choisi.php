<?php
	if (!isset($_SESSION['authentification_OK']) || (($_SESSION["admin"] == false) && ($_SESSION["moderateur"] == false))){
		echo "<script type='text/javascript'>document.location.replace('index.php?page=login');</script>";
    }
    
    if (isset($_GET['user'])) {
        $user_id = $_GET['user'];
        $user_recup = new Utilisateur();
        $user = $user_recup->recupUser($user_id);
        $nom = $user['nom'];
        $prenom = $user['prenom'];
        $email = $user['email'];
        $adresse = $user['adresse'];
        $moderateur = $user['moderateur'];
        $password_encr = $user['password'];
        $password = md5($password_encr);
    }

    $message_info = "";
    $message_upload = false;
    $insert = "";

    $info_suppr = "";
    $message_suppri = false;
    $supprimed = "";

    if (!empty($_POST['change_informations'])) {
        $moderateur_chk = intval(isset($_POST['moderateur']));
        
        if ((!empty($_POST['nom']) && $_POST['nom'] != $nom) || (!empty($_POST['prenom']) && $_POST['prenom'] != $prenom) || (!empty($_POST['email']) && $_POST['email'] != $email) || (!empty($_POST['adresse']) && $_POST['adresse'] != $adresse) || (!empty($_POST['password1']) && $_POST['password1'] != $password) || (!empty($_POST['password2']) && $_POST['password2'] != $password) || ($moderateur_chk != $moderateur)){

            $nom_changed = htmlspecialchars($_POST['nom']);
            $prenom_changed = htmlspecialchars($_POST['prenom']);
            $email_changed = htmlspecialchars($_POST['email']);
            $adresse_changed = htmlspecialchars($_POST['adresse']);
            $moderateur_changed = $moderateur_chk;
            $password1 = htmlspecialchars($_POST['password1']);
            $password2 = htmlspecialchars($_POST['password2']);

            if ($password1 == $password2) {
                $password_changed = md5($password1);
                
                $change_user = new Utilisateur();
                $insert = $change_user->majUser($nom_changed, $prenom_changed, $email_changed, $adresse_changed, $moderateur_changed, $password_changed, $user_id);
                            
                if ($insert) {
                    $message_upload = true;
                    $message_info = "Vos données ont bien été mises à jour.";
                } else {
                    $message_upload = true;
                    $message_info = "Erreur : veuillez réessayer.";
                }
            } else {
                $message_upload = true;
                $message_info = "Erreur : Veuillez saisir des mots de passe identiques.";
            }
        }
    }

    if (!empty($_POST["supprimer"])) {
        $suppr_user = new Utilisateur();
        $supprimed = $suppr_user->supprUser($user_id);

        if ($supprimed) {
            $info_suppr = "L'utilisateur a bien été supprimé.";
            $message_suppri = true;
        } else {
            $info_suppr = "L'utilisateur n'a pas été supprimé, réessayez plus tard.";
            $message_suppri = true;
        }
    }
?>

<section class="back_office_container">
    <div class="container">
        <div class="row justify-content-between padding-bottom align-items-start">
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="formulaire">
                    <h2>Panier de l'utilisateur</h2>

                    <?php
                        $product = new Panier($_GET["user"]);
                        $products_in_panier = $product->product();
                        // var_dump($products_in_panier);

                        $number_of_articles = 0;
                        $price_total = 0;

                    foreach ($products_in_panier as $produit) {
                        $numb = $product->createProducts($produit);

                        echo '
                        <div class="produit">
                            <h5>'.$produit["titre"].'</h5>
                            <h5 class="prix">'.$produit["prix"].'€</h5>
                            <div class="align-img">
                                <div class="img">
                                    <img src="'.$produit["image"].'">
                                </div>
                                <div class="align-items">
                                    <p class="quantity">Quantité : '.$numb["quantity"].'</p>
                                </div>
                            </div>
                        </div>
                        ';

                        $number_of_articles += $numb['quantity'];
                        $price_total += ($produit["prix"] * $numb['quantity']);
                    }

                    ?>

                    <div class="line"></div>
                    <p class="total">Sous-total (<?php echo $number_of_articles ?> article(s)) : <?php echo $price_total ?> €</p>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="formulaire">
                    <form method="POST" action="backoffice.php?page=utilisateur-choisi<?php echo "&&user=$user_id" ?>">
                        <h2>Modifier les informations de l'utilisateur</h2>
                        <div class="form-group">
                            <label for="nom">Nom</label>
                            <input type="text" class="form-control" name="nom" value="<?php echo $nom ?>">
                        </div>
                        <div class="form-group">
                            <label for="nom">Prénom</label>
                            <input type="text" class="form-control" name="prenom" value="<?php echo $prenom ?>">
                        </div>
                        <div class="form-group">
                            <label for="nom">Email</label>
                            <input type="email" class="form-control" name="email" value="<?php echo $email ?>">
                        </div>
                        <div class="form-group">
                            <label for="nom">Adresse</label>
                            <input type="text" class="form-control" name="adresse" value="<?php echo $adresse ?>">
                        </div>
                        <div class="form-group">
                            <label for="nom">Modifier le mot de passe</label>
                            <input type="password" class="form-control" name="password1">
                        </div>
                        <div class="form-group">
                            <label for="nom">Vérifiez le mot de passe</label>
                            <input type="password" class="form-control" name="password2">
                        </div>

                        <?php
                        if ($_SESSION["admin"] == true) {
                            echo '
                            <div class="form-group">
                                <input type="checkbox" class="custom-control-input" id="customControlAutosizing" name="moderateur" value="1"';
                                
                            if ($moderateur == "1"){
                                echo "checked";
                            }
                            
                            echo '>
                                <label class="custom-control-label" for="customControlAutosizing">Cet utilisateur est modérateur</label>
                            </div>';
                            
                        }
                        ?>
                        <button type="submit" class="btn btn-primary button-backoffice" name="change_informations" value="change">Modifier les informations</button>
                    </form>
                    <?php
                        if ($message_upload) {
                            echo "<p class='info-create-user'>$message_info</p>";
                        }
                    ?>
                </div>
                
                <div class="formulaire">
                    <form method="POST" action="backoffice.php?page=utilisateur-choisi<?php echo "&&user=$user_id" ?>">
                        <div class="form-group">
                            <h2>Supprimer l'utilisateur</h2>
                            <button type="submit" class="btn btn-primary button-backoffice" name="supprimer" value="supprimer">Supprimer l'utilisateur</button>
                        </div>
                    </form>
                    <?php
                        if ($message_suppri) {
                            echo "<p class='info-create-user'>$info_suppr</p>";
                        }
                    ?>
                </div>
            </div>
        </div>
    </section>