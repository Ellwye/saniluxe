<section id="faq">
    <div class="container">
        <h2>Les questions fréquentes</h2>

        <h3>Les bidets</h3>

        <button class="accordion">Comment installer un bidet ?
            <div class="button-faq">
                <i class="fas fa-plus"></i>
            </div>
        </button>
        <div class="panel">
            <div class="padding-panel">
                <p>
                    Référez-vous à la notice de montage livrée avec votre produit.
                </p>
                <p>
                    <span class="important">Généralement, l'on procède comme suit :</span> <br>
                    Tracez au crayon l'emplacement du bidet au sol. Indiquez l'emplacement des conduites d'alimentation d'eau chaude et froide puis installez les raccordements flexibles. Installez les raccords du siphon en « P ». Installez les raccords du mélangeur ou du bidet (selon le modèle) ainsi que l'évacuation mécanique.
                </p>
            </div>
        </div>
        
        <button class="accordion">À quoi sert un bidet ?
            <div class="button-faq">
                <i class="fas fa-plus"></i>
            </div>
        </button>
        <div class="panel">
            <div class="padding-panel">
                <p>
                    Le bidet sert principalement à l'hygiène des parties génitales et de l'anus. ... Le bidet est à l'origine essentiellement destiné à la toilette intime. Il peut aussi servir à laver d'autres parties du corps humain, tels que les pieds et les cheveux ou pourquoi pas son chien ou tout autre animal de compagnie.
                </p>
            </div>
        </div>
        
        <h3>Les douches</h3>

        <button class="accordion">Comment installer une douche ?
            <div class="button-faq">
                <i class="fas fa-plus"></i>
            </div>
        </button>
        <div class="panel">
            <div class="padding-panel">
                <p>
                    Étape 1 : Positionnez le receveur de votre douche classique <br>
                    Étape 2 : Installez la bonde et le joint d'étanchéité de votre douche classique<br>
                    Étape 3 : Assemblez les manchons au flexible d'évacuation de votre douche classique<br>
                    Étape 4 : Fixez le flexible d'évacuation à la bonde de votre douche classique et raccordez-le<br>
                    Étape 5 : Fixez le receveur de votre douche classique aux murs
                </p>
            </div>
        </div>

        <h3>Les toilettes</h3>

        <button class="accordion">Qu'est-ce qu'un WC lavant (japonais) ?
            <div class="button-faq">
                <i class="fas fa-plus"></i>
            </div>
        </button>
        <div class="panel">
            <div class="padding-panel">
                <p>
                    Les WC lavants, dits aussi toilettes japonaises ou washlets, ont été créés en Suisse en 1957. Leur spécificité principale est le rinçage afin de favoriser l'hygiène intime.
                </p>
            </div>
        </div>

        <button class="accordion">Quels sont les avantages d'un WC lavant ?
            <div class="button-faq">
                <i class="fas fa-plus"></i>
            </div>
        </button>
        <div class="panel">
            <div class="padding-panel">
                <p>
                    <span class="important">Les avantages d'un WC lavant sont nombreux :</span> <br>
                    <i class="material-icons">keyboard_arrow_right</i> Rinçage naturel et non agressif pour le corps puisqu'il est fait uniquement avec de l'eau.<br>
                    <i class="material-icons">keyboard_arrow_right</i> Hygiène renforcée.<br>
                    <i class="material-icons">keyboard_arrow_right</i> Sensation de propreté réelle, de fraîcheur et de bien-être tout au long de la journée.<br>
                    <i class="material-icons">keyboard_arrow_right</i> Confort d'utilisation lié au chauffage de l'eau et de l'abattant.<br>
                    <i class="material-icons">keyboard_arrow_right</i> Un WC lavant peut aussi participer au maintien à domicile des personnes âgées, en augmentant leur autonomie et en réduisant les problèmes de mobilité et d'accessibilité à un lavabo ou une douche après utilisation, du fait de la fonctionnalité rinçage intégrée au WC.
                </p>
            </div>
        </div>

        <button class="accordion">Comment entretenir et nettoyer un WC lavant ?
            <div class="button-faq">
                <i class="fas fa-plus"></i>
            </div>
        </button>
        
        <div class="panel">
            <div class="padding-panel">
                <p>
                    On utilisera les mêmes produits que pour une cuvette classique. En revanche, en fonction des régions, il faut être vigilant à la dureté de l'eau qui pourrait entraîner des dépôts de calcaire plus ou moins importants, et prévoir un adoucisseur d'eau. Un détartrage peut être envisagé tous les deux-trois ans, si besoin.
                </p>
            </div>
        </div>

    </div>
</section>