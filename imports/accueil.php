<?php
    $produit = new Nouveautes();
?>

<section id="banner">
    <div class="title">
        <h1>Des produits haut de gamme uniques</h1>
        <p class="subtitle">Découvrez notre collection de bains, douches, toilettes, robinets, lavabos et accessoires.</p>
    </div>
    <img src="assets/img/salle-bains.jpg" alt="Photographie d'une salle de bains avec baignoir design" id="img-1">
    <img src="assets/img/salle-de-bains-blanche.jpg" alt="Photographie d'une salle de bains avec baignoir design" class="hidden" id="img-2">
    <img src="assets/img/salle-de-bains-moderne.jpg" alt="Photographie d'une salle de bains avec baignoir design" class="hidden"  id="img-3">

    <div class="button-banner-prev" id="prev"><i class="material-icons">keyboard_arrow_left</i></div>
    <div class="button-banner-next" id="next"><i class="material-icons">keyboard_arrow_right</i></div>

</section>

<section id="marque">
    <div class="container">
        <div class="align">
            <h2>
                Spécialiste de la salle de bain de luxe<br>
                 depuis 1950
            </h2>
            <div class="traits">
                <div class="anim-p" id="presentation">
                    <p>
                        SANILUX est votre partenaire privilégié de fourniture de robinetterie de luxe, de sanitaires et accessoires de salle de bain design. Notre équipe, professionnelle et attentive à tous vos besoins, saura vous guider dans vos choix, en privilégiant toujours la communication et le service.<br>
                        <br>
                        Que vous rêviez d'une salle de bain exceptionnelle au style classique, retro, contemporain ou bien ultra luxueuse, vous trouverez dans nos collections les équipements et accessoires correspondant à vos exigences, tant en terme de qualité que de design. Notre équipe, professionnelle et attentive à vos envies, saura vous guider dans vos choix et mettra à votre service son expertise et son inspiration.<br>
                        <br>
                        Pour l'équipement de votre salle de bain de luxe, faites confiance à SANILUX, spécialiste depuis 1950 de la fourniture de sanitaires design, de robinetterie haut de gamme et de tous types d'accessoires pour salles de bain de luxe.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="articles">
    <div class="container">
        <h2>Les nouveautés</h2>
        <div class="cards-alignement">

            <?php
                $result = $produit->produits_en_stock();
                $array_lenght = count($result);

                for ($i = $array_lenght -1; $i >= ($array_lenght - 3); $i--) {
        
                    echo '<div class="card">
                                <div class="padding-card">
                                    <div class="img-card"><img src="'.$result[$i]["image"].'" alt="'.$result[$i]["titre"].'"></div>
                                    <h4>'.$result[$i]["titre"].'</h4>
                                    <p class="card-text">
                                        '.$result[$i]["description"].'
                                    </p>
                                    <div class="align-buttons">
                                    <a href="index.php?page=produit&&categorie='.$result[$i]["categorie"].'&&id='.$result[$i]["id"].'">Découvrir <i class="material-icons">keyboard_arrow_right</i></a>
                                    <input type="hidden" name="stock" value="'.$result[$i]["stock"].'">
                                    <button type="button" class="add-cart" name="cart_add" value="'.$result[$i]["id"].'">
                                        <i class="fas fa-cart-plus"></i>
                                    </button>
                                    </div>
                                </div>
                            </div>';
                }
            ?>

        </div>
    </div>
</section>