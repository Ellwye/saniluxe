<!-- Button menu for mobile -->
<button id="menu-hamburger" type="button"><img src="assets/img/menu.svg"
            alt="Icone ouvrant le menu version mobile"></button>

    <!-- Header -->
    <header>
        <!-- First part of menu (identity) -->
        <!-- Button for menu mobile -->
        <div class="menu-mobile-close">
            <h5>MENU</h5>
            <button type="button" id="button-close-menu">
                <i class="material-icons">close</i>
            </button>
        </div>

        <div class="identity-menu">
            <div class="identity-logo">
                <a href="index.php?page=accueil">
                    <img src="assets/img/logotype-sanilux.svg" alt="Logotype Sanilux">
                </a>
            </div>

            <div class="icons-menu">
                <!-- Search -->
                <div id="search">
                    <form id="search-bar" method="post" action="search.php">
                        <input class="input-text" type="text" name="search" maxlength="100" value=""
                            placeholder="Que recherchez-vous ?" />
                        <button type="submit" id="search-button-submit">
                            <i class="material-icons icon-menu">search</i>
                        </button>
                    </form>
                </div>

                <div class="align-icons-menu">
                    <!-- Shopping cart -->
                    <div class="margin-buttons-menu">
                        <button type="button" class="buttons-menu"><a href="index.php?page=panier"><i
                                class="material-icons icon-menu">shopping_cart</i><span class="badge badge-light current_quantity"></span></a></button>
                    </div>

                    <!-- User connection / registration -->
                    <div class="margin-buttons-menu">
                        <button type="button" class="buttons-menu"><a href="index.php?page=user"><i class="fas fa-user icon-menu"></a></i></button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Nav -->
        <nav>
            <div class="navigation-menu">

                <!-- Lists items for navigation -->
                <ul class="navbar-style">

                    <li>
                        <a href="index.php?page=accueil" <?php
                        if (isset($_GET["page"])) {
                            if ($_GET["page"] == "accueil") {
                                echo 'class="link-active"';
                            }
                        }
                        ?>>Accueil</a>
                    </li>
                    <li class="line-menu-separator"></li>
                    <li>
                        <a href="index.php?page=collection" <?php
                        if (isset($_GET["page"])) {
                            if ($_GET["page"] == "collection" || $_GET["page"] == "produit") {
                                echo 'class="link-active"';
                            }
                        }
                        ?>>Collection</a>
                    </li>
                    <li class="line-menu-separator"></li>
                    <li>
                        <a href="index.php?page=faq" <?php
                        if (isset($_GET["page"])) {
                            if ($_GET["page"] == "faq") {
                                echo 'class="link-active"';
                            }
                        }
                        ?>>FAQ</a>
                    </li>
                    <li class="line-menu-separator"></li>
                    <li>
                        <a href="index.php?page=contact" <?php
                        if (isset($_GET["page"])) {
                            if ($_GET["page"] == "contact") {
                                echo 'class="link-active"';
                            }
                        }
                        ?>>Contact</a>
                    </li>

                </ul>
                <!-- End list items for navigation -->

            </div>
        </nav>
        <!-- End nav -->

    </header>
    <!-- End header -->
