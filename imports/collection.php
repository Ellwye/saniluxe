<div class="container">
    <div class="fil_ariane">
        <p><a href="index.php?page=accueil">Accueil</a> > <span class="important">Collection</span></p>
    </div>

    <div class="alignement-produits">

        <!-- Catégories -->
        <section id="categories">
            <div class="bloc_categories">
                <h2>Collection</h2>
                <?php
                    $dbh = new BDD();
                    $bdd = $dbh->getConnection();

                    $stmt = $bdd->query('SELECT * FROM categories');

                    foreach ($stmt as $categorie) {
                        $class = "";
                        if (isset($_GET["categorie"])) {
                            if ($_GET["categorie"] == $categorie["name"]) {
                                $class = 'class="actif"';
                            }
                        }
                        echo '
                            <a href="index.php?page=collection&&categorie='.$categorie["name"].'" '.$class.'>'.$categorie["name"].'</a>
                        ';
                    }
                ?>
            </div>
        </section>

        <!-- Articles -->
        <section id="articles">
            <div class="articles-container">
                <div class="cards-alignement">

                    <?php
                       $page = $_GET["page"];

                       $produit = new Collection;

                       if ($page == "collection" && isset($_GET["categorie"])) {
                            $result = $produit->produits_collection_categorie($_GET["categorie"]);
                            foreach ($result as $item) {
                                echo '
                                <div class="card">
                                    <div class="padding-card">
                                        <div class="img-card"><img src="'.$item["image"].'" alt="'.$item["titre"].'"></div>
                                        <h4>'.$item["titre"].'</h4>
                                        <p class="card-text">
                                            '.$item["description"].'
                                        </p>
                                        <div class="align-buttons">
                                          <a href="index.php?page=produit&&categorie='.$item["categorie"].'&&id='.$item["id"].'">Découvrir <i class="material-icons">keyboard_arrow_right</i></a>
                                          <input type="hidden" name="stock" value="'.$item["stock"].'">
                                          <button type="button" class="add-cart" name="cart_add" value="'.$item["id"].'">
                                              <i class="fas fa-cart-plus"></i>
                                          </button>

                                        </div>
                                    </div>
                                </div>
                                ';
                            }

                        } else {
                            $result = $produit->produits_collection();
                            foreach ($result as $item) {
                                echo '
                                <div class="card">
                                    <div class="padding-card">
                                        <div class="img-card"><img src="'.$item["image"].'" alt="'.$item["titre"].'"></div>
                                        <h4>'.$item["titre"].'</h4>
                                        <p class="card-text">
                                            '.$item["description"].'
                                        </p>
                                        <div class="align-buttons">
                                        <a href="index.php?page=produit&&categorie='.$item["categorie"].'&&id='.$item["id"].'">Découvrir <i class="material-icons">keyboard_arrow_right</i></a>
                                        <input type="hidden" name="stock" value="'.$item["stock"].'">
                                        <button type="button" class="add-cart" name="cart_add" value="'.$item["id"].'">
                                            <i class="fas fa-cart-plus"></i>
                                        </button>
                                        </div>
                                    </div>
                                </div>
                                ';
                            }
                       }
                    ?>

                </div>
            </div>
        </section>

    </div>
</div>
