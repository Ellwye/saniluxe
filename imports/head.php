<script type="text/javascript" src="https://cdn.weglot.com/weglot.min.js"></script>
<script>
    Weglot.initialize({
        api_key: 'wg_cd975bb5622f99abe2859413dd0347a30'
    });
</script>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Sanilux | Sanitaires haut de gamme</title>

    <!-- Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/img/favicon.ico">
    <link rel="shortcut icon" href="assets/img/favicon.ico">

    <!-- Fonts import -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">

    <!-- Styles imports -->
    <link href="assets/css/styles.min.css" type="text/css" rel="stylesheet">
    <!-- Global metas -->
    <meta name="description"
        content="SANILUX est votre partenaire privilégié de fourniture de robinetterie de luxe,
de sanitaires et accessoires de salle de bain design. Notre équipe,
professionnelle et attentive à tous vos besoins, saura vous guider dans vos
choix, en privilégiant toujours la communication et le service.">
    <meta name="author" content="Sanilux">
    <meta name="keywords" content="sanitaires de luxe, sanitaires design, douche, toilettes japonaises, baignoire, robinets, bidet, toilettes, lavabo, acheter toilettes, acheter douche, acheter baignoire, acheter baignoire, acheter robinet, acheter bidet, acheter lavabo, sanitaires haut de gamme">

    <!-- Metas Facebook & Twitter for sharing -->
    <meta property="og:url" content="https://www.sanilux.fr" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Sanilux" />
    <meta property="og:description"
        content="SANILUX est votre partenaire privilégié de fourniture de robinetterie de luxe,
de sanitaires et accessoires de salle de bain design. Notre équipe,
professionnelle et attentive à tous vos besoins, saura vous guider dans vos
choix, en privilégiant toujours la communication et le service." />
    <meta property="og:image" content="assets/img/logotype-sanilux.svg" />
    <meta name="twitter:card"
        content="SANILUX est votre partenaire privilégié de fourniture de robinetterie de luxe,
de sanitaires et accessoires de salle de bain design. Notre équipe,
professionnelle et attentive à tous vos besoins, saura vous guider dans vos
choix, en privilégiant toujours la communication et le service." />
    <meta name="twitter:site" content="https://www.sanilux.fr" />
    <meta name="twitter:creator" content="Sanilux" />
</head>
