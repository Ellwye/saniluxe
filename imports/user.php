<?php

$dbh = new BDD();
$bdd = $dbh->getConnection();

if(!isset($_SESSION['authentification_OK'])){
    echo "<script type='text/javascript'>document.location.replace('index.php?page=login');</script>";
}


// Récupération des informations de l'utilisateur
$id = $_SESSION['id'];
$response = $bdd->query("SELECT * FROM user WHERE id = '$id' ");
$user = $response->fetch();
$nom = $user['nom'];
$prenom = $user['prenom'];
$email = $user['email'];
$adresse = $user['adresse'];
$password = $user['password'];
$response->closeCursor();

$message_info = "";
$message_upload = false;
$insert = "";

$password_upload = false;
$password_info = "";
$insert_password = "";

if ((!empty($_POST['nom']) && $_POST['nom'] != $nom) || (!empty($_POST['prenom']) && $_POST['prenom'] != $prenom) || (!empty($_POST['email']) && $_POST['email'] != $email) || (!empty($_POST['adresse']) && $_POST['adresse'] != $adresse)) {
    $nom = htmlspecialchars($_POST['nom']);
    $prenom = htmlspecialchars($_POST['prenom']);
    $email = htmlspecialchars($_POST['email']);
    $adresse = htmlspecialchars($_POST['adresse']);

    // Mise à jour dans la BDD
    // $envoi_bdd = $bdd->prepare("UPDATE user SET (nom = ':nom', prenom = ':prenom', email = ':email', adresse = ':adresse', password = ':password') WHERE id = '$id' ");
    $envoi_bdd = $bdd->prepare("UPDATE `user` SET `nom`=:nom,`prenom`=:prenom,`email`=:email,`adresse`=:adresse WHERE `id`='$id'");
    $envoi_bdd->bindParam(':nom',$nom);
    $envoi_bdd->bindParam(':prenom',$prenom);
    $envoi_bdd->bindParam(':email',$email);
    $envoi_bdd->bindParam(':adresse',$adresse);
    $insert = $envoi_bdd->execute();
    if ($insert) {
        $message_upload = true;
        $message_info = "Vos données ont bien été mises à jour.";
    } else {
        $message_upload = true;
        $message_info = "Erreur : veuillez réessayer.";
    }
}


if (!empty($_POST['password1']) || !empty($_POST['password2'])) {
    $password_changed1 = htmlspecialchars($_POST['password1']);
    $password_changed2 = htmlspecialchars($_POST['password2']);

    if ($password_changed1 == $password_changed2) {
        $password_encrypt = md5($password_changed1);

        if ($password_encrypt != $password) {
            $password = $password_encrypt;
            $envoi_bdd = $bdd->prepare("UPDATE `user` SET `password`=:password WHERE `id`='$id'");
            $envoi_bdd->bindParam(':password',$password);
            $insert_password = $envoi_bdd->execute();
    
            if ($insert_password) {
                $password_upload = true;
                $password_info = "Mot de passe modifié avec succès";
            }
        } else {
            $password_upload = true;
            $password_info = "Choisissez un mot de passe différent du mot de passe actuel.";
        }

    } else {
        $password_upload = true;
        $password_info = "Mots de passe non identiques.";
    }
}

?>
<section id="user">
    <div class="container">
        <h2>Bonjour <?php echo $prenom ?></h2>
        <a href="index.php?page=panier" class="buttons buttons-style">Mon panier</a>

        <?php
	    if (($_SESSION["admin"] == true) || ($_SESSION["moderateur"] == true)){
		echo '<a href="backoffice.php?page=utilisateurs-back-office" class="buttons buttons-style">Espace back-office</a>';
        }
        ?>

        <a href="index.php?page=sign_out" class="buttons buttons-style">Déconnexion</a>

        <div class="background">
            <h2>Modifier mes informations</h2>
            <form action="index.php?page=user" method="POST">
                <label for="nom">Nom</label>
                <input type="text" name="nom" value="<?php echo $nom ?>" >

                <label for="prenom">Prénom</label>
                <input type="text" name="prenom" value="<?php echo $prenom ?>" >

                <label for="email">Email</label>
                <input type="email" name="email" value="<?php echo $email ?>" >

                <label for="adresse">Adresse</label>
                <input type="text" name="adresse" value="<?php echo $adresse ?>" >

                <button type="submit" class="buttons buttons-style" name="register">Modifier</button>
            </form>
            <div class="message_info">
                <p><?php if ($message_upload) {
                    echo $message_info;
                } ?></p>
            </div>
        </div>

        <div class="background">
            <h2>Changer mon mot de passe</h2>
            <form action="index.php?page=user" method="POST">
                <label for="password1">Mot de passe</label>
                <input type="password" name="password1" placeholder="Choisissez un mot de passe" required>

                <label for="password2">Vérifier le mot de passe</label>
                <input type="password" name="password2" placeholder="Vérifier votre mot de passe" required>

                <button type="submit" class="buttons buttons-style" name="register">Modifier</button>
            </form>
            <div class="message_info">
                <p><?php if ($password_upload) {
                    echo $password_info;
                } ?></p>
            </div>
        </div>
    </div>
</section>