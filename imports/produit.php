<?php
    $id_produit = $_GET["id"];
    $produit = new Produit($id_produit);
    $produit->recherche_produit_by_id();
    // foreach($result as $row) {
    //     echo "<pre>";
    //     var_dump($row);
    //     echo "</pre>";
    // }
?>

<div class="container">
    <div class="fil_ariane">
        <p><a href="index.php?page=accueil">Accueil</a> > <a href="index.php?page=collection">Collection</a> > <span class="important"><?php echo $produit->titre; ?></span></p>
    </div>


    <div class="alignement-produits">

<!-- Catégories -->
<section id="categories">
            <div class="bloc_categories">
                <h2>Collection</h2>
                <?php
                    $dbh = new BDD();
                    $bdd = $dbh->getConnection();

                    $stmt = $bdd->query('SELECT * FROM categories');

                    foreach ($stmt as $categorie) {
                        $class = "";
                        if (isset($_GET["categorie"])) {
                            if ($_GET["categorie"] == $categorie["name"]) {
                                $class = 'class="actif"';
                            }
                        }
                        echo '
                            <a href="index.php?page=collection&&categorie='.$categorie["name"].'" '.$class.'>'.$categorie["name"].'</a>
                        ';
                    }
                ?>
            </div>
        </section>

<!-- Articles -->
<?php
    $result = $produit->recherche_produit_by_id();

    echo '
        <section id="article">
            <div class="article-background">
                <img src="'.$result["image"].'" alt="'.$result["titre"].'"></img>
                <div class="title_price">
                    <h5>'.$result["titre"].'</h5>
                    <p class="price">'.$result["prix"].' €</p>
                </div>
                <p>'.$result["description"].'</p>
                <form method="POST" action="index.php?page=panier" class="add-panier">
                    <button class="buttons buttons-style-button" name="cart_add" value="'.$result["id"].'">
                        <i class="fas fa-cart-plus"></i> Ajouter au panier
                    </button>
                </form>
            </div>
        </section>
    ';
?>

</div>


</div>