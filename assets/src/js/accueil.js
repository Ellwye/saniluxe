$(document).ready(function () {
    if( $('#presentation').length > 0 ){
      var presentation_block = document.getElementById("presentation");
      var hauteur_presentation = presentation_block.getBoundingClientRect();

      $(document).scroll(function () {
          if (window.scrollY > hauteur_presentation.height + hauteur_presentation.height/2) {
              $('.anim-p p').css("transform", "translateX(0)");
          }
      });
    }
});
