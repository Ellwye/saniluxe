"use strict";

document.addEventListener('DOMContentLoaded', function () {
  var btn_scroll_top = document.getElementById("button-scroll-top");

  if (window.scrollY > 1000) {
    btn_scroll_top.classList.remove("hidden_btn");
    btn_scroll_top.classList.add("visible_btn");
  } else {
    btn_scroll_top.classList.remove("visible_btn");
    btn_scroll_top.classList.add("hidden_btn");
  } // Lorsque je scroll, executer la fonction qui permet de cacher ou de rendre visible mon bouton et mes div


  document.onscroll = function () {
    /* Code for btn scroll top */
    if (window.scrollY > 1000) {
      btn_scroll_top.classList.remove("hidden_btn");
      btn_scroll_top.classList.add("visible_btn");
    } else {
      btn_scroll_top.classList.remove("visible_btn");
      btn_scroll_top.classList.add("hidden_btn");
    }
  }; // Au clic sur le bouton, remonter en haut de la page


  btn_scroll_top.addEventListener("click", function () {
    window.scrollTo(0, 0);
  });
});