$(document).ready(function () {
    var acc = $(".accordion");

    for (var i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {

            $(this).toggleClass("active"); // toggle cache ou montre un élément suivant s'il est caché ou visible

            var panel = this.nextElementSibling; // renvoie l'élément suivant (noeud suivant)

            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px"; // panel.scrollHeight renvoie la hauteur de l'élément
            }
        });
    }
});
