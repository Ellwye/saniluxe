$('document').ready(function () {
    $('#modalProduct').on('show.bs.modal', function (event) {
        $('#modalProduct .btn-add-to-card').off('click');
        $('#modalProduct .btn-add-to-card').on('click', function () {
            var idProduit = $(this).val();
            var Quantite = $(this).parent().find('#quantite option:selected').val();
            $.ajax({
                url: './AJAX/add_to_cart.php',
                type: 'POST',
                data: {
                    idProduit: idProduit,
                    Quantite: Quantite
                },
                dataType: 'JSON',
                success: function (callback, statut) {
                    if (callback.success) {
                        $('#modalProduct .actions').hide();
                        $('<p class="success">Le produit a bien été ajouté au panier</p>').appendTo('#modalProduct .modal-body .contenu-product .produit');
                        updateCartFront();
                        updateProductQuantity();
                    } else {
                        $('<p>Le produit n\'a pas été ajouté au panier, réessayez.</p>').appendTo('#modalProduct .modal-body .contenu-product .produit');
                    }
                }
            });
        });
    });
    $('.add-cart').on('click', function () {
        $('#modalProduct .actions').show();
        $('#modalProduct .modal-body .contenu-product .produit p').remove();
        var titleProduct = $(this).parent().parent().find('h4').html();
        var descProduct = $(this).parent().parent().find('.card-text').html();
        var imgProduct = $(this).parent().parent().find('.img-card').html();
        var id_product = $(this).val();
        var stockProduct = $(this).parent().parent().find('input[type=hidden]').val();
        $('#modalProduct .modal-header h4').html(titleProduct);
        $('#modalProduct .modal-body .image-product').html(imgProduct);
        $('#modalProduct .modal-body .contenu-product .produit').html("<p>"+descProduct+"</p>");
        $('#modalProduct .btn-add-to-card').val($(this).val())
        $('#modalProduct').modal('show');

        updateProductQuantity(id_product, stockProduct);
    });
    updateCartFront();
})

function updateCartFront() {
    $.ajax({
        url: './AJAX/get_cart_badge.php',
        type: 'POST',
        data: {},
        dataType: 'JSON',
        success: function (callback, statut) {
            var currentQtt = $('.current_quantity').html();
            currentQtt = callback.data.total;
            $('.current_quantity').html(currentQtt);
        }
    });
}

function updateProductQuantity(id_product, quantity_product_disponible) {
    $.ajax({
        url: './AJAX/update_product_quantity.php',
        type: 'POST',
        data: {},
        dataType: 'JSON',
        success: function (callback, statut) {
            if (callback.data.length > 0) {
                callback.data.forEach(element => {
                    if (id_product == element.id_product) {
                        quantity_product_disponible = parseInt(quantity_product_disponible) - parseInt(element.quantity);
                    }
                    var select_options = "";
                    if (quantity_product_disponible > 0) {
                        for(i=1; i<=quantity_product_disponible; i++) {
                            select_options += "<option value='"+i+"'>"+i+"</option>";
                        }
                        $('#modalProduct .modal-body .contenu-product .actions #quantite').html(select_options);
                    } else {
                        $('#modalProduct .modal-body .contenu-product .actions #quantite').html("<option value=''>Vous avez ajouté le nombre maximum d'article(s) disponible(s).</option>");
                    }
                });
            } else {
                var select_options = "";
                for(i=1; i<=quantity_product_disponible; i++) {
                    select_options += "<option value='"+i+"'>"+i+"</option>";
                }
                $('#modalProduct .modal-body .contenu-product .actions #quantite').html(select_options);
            }
        }
    });
}