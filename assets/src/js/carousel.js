$(document).ready(function(){
    
    var carrousel = $('#banner'); // on cible le bloc du carrousel
    var img = $('#banner img'); // on cible les images contenues dans le carrousel
    var indexImg = img.length - 1; // on définit l'index du dernier élément
    var i = 0; // on initialise un compteur
    var currentImg = img.eq(i); // enfin, on cible l'image courante, qui possède l'index i (0 pour l'instant)
    
    $('#next').click(function(){ // image suivante
    
        i++; // on incrémente le compteur
    
        if( i <= indexImg ){

            img.addClass("hidden");
            currentImg = img.eq(i); // on définit la nouvelle image
            currentImg.removeClass("hidden"); // puis on l'affiche
        }
        else{
            i = indexImg;
        }
    
    });
    
    $('#prev').click(function(){ // image précédente
    
        i--; // on décrémente le compteur, puis on réalise la même chose que pour la fonction "suivante"
    
        if( i >= 0 ){
           img.addClass("hidden");
            currentImg = img.eq(i);
            currentImg.removeClass("hidden");
            
        }
        else{
            i = 0;
        }
    
    });
    
    function slideImg(){
        setTimeout(function(){ // on utilise une fonction anonyme
                            
        if(i < indexImg){ // si le compteur est inférieur au dernier index
            i++; // on l'incrémente
        }
        else{ // sinon, on le remet à 0 (première image)
            i = 0;
        }
    
        img.addClass("hidden");
        currentImg = img.eq(i);
        currentImg.removeClass("hidden");
    
        slideImg(); // on oublie pas de relancer la fonction à la fin
    
        }, 8000); // on définit l'intervalle à 7000 millisecondes (7s)
    }
    
    slideImg(); // enfin, on lance la fonction une première fois
    
    });
    